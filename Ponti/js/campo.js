/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees=[
  {
    "fruit_id": 61686,
    "name": "arancia vaniglia",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1584093722/fruits/biorfarm-icon-arancia-vaniglia_tree.png",
    "number": 250,
    "co2_unit": 0.07,
    "farmer_name": "Azienda Agricola Mirabella",
    "location_country": "Burgio, Sicilia",
    "location_province": "Burgio, Agrigento (AG)",
    "selected": true,
    "co2": "17,50"
  },
  {
    "fruit_id": 61687,
    "name": "arancia washington navel",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1584093722/fruits/biorfarm-icon-arancia-washington-navel_tree.png",
    "number": 250,
    "co2_unit": 0.07,
    "farmer_name": "Azienda Agricola Mirabella",
    "location_country": "Burgio, Sicilia",
    "location_province": "Burgio, Agrigento (AG)",
    "selected": true,
    "co2": "17,50"
  }
 ]