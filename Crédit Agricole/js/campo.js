/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 84238,
    "name": "albicocca portici",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1604403563/fruits/biorfarm-icon-albicocca-portici-dell-orto_tree.png",
    "number": 150,
    "co2_unit": 0.07,
    "farmer_name": "Famiglia Dell'Orto",
    "location_country": "Piana del Sele, Campania",
    "location_province": "Piana del Sele, Salerno (SA)",
    "selected": true,
    "co2": "10,50"
  },
  {
    "fruit_id": 167651,
    "name": "arancia biondo della riviera dei gelsomini",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1639568082/fruits/biorfarm-icon-arancia-biondo_tree.png",
    "number": 101,
    "co2_unit": 0.07,
    "farmer_name": "Giovanni Zappavigna",
    "location_country": "Ardore, Calabria",
    "location_province": "Ardore, Reggio Calabria (RC)",
    "selected": true,
    "co2": "7,07"
  },
  {
    "fruit_id": 75554,
    "name": "arancia biondo di caulonia",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595943654/fruits/biorfarm-icon-arancia-biondo-di-caulonia_tree.png",
    "number": 49,
    "co2_unit": 0.07,
    "farmer_name": "Ilaria Campisi",
    "location_country": "Caulonia, Calabria",
    "location_province": "Caulonia, Reggio Calabria (RC)",
    "selected": true,
    "co2": "3,43"
  },
  {
    "fruit_id": 75225,
    "name": "arancia tarocco sant'alfio",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595581977/fruits/biorfarm-icon-arancia-tarocco-sant-alfio_tree.png",
    "number": 100,
    "co2_unit": 0.07,
    "farmer_name": "Emanuele Calvo",
    "location_country": "Mineo, Sicilia",
    "location_province": "Mineo, Catania (CT)",
    "selected": true,
    "co2": "7,00"
  },
  {
    "fruit_id": 62374,
    "name": "kiwi hayward",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1581586234/fruits/biorfarm-icon-kiwi_tree.png",
    "number": 50,
    "co2_unit": 0.00115,
    "farmer_name": "Azienda Agricola Masseria Cesaro",
    "location_country": "Teverola, Campania",
    "location_province": "Teverola, Caserta (CE)",
    "selected": true,
    "co2": "0,06"
  },
  {
    "fruit_id": 38777,
    "name": "limone verdello",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1572517416/fruits/biorfarm-icon-limone_verdello_tree.png",
    "number": 150,
    "co2_unit": 0.068,
    "farmer_name": "Giovanni Zappavigna",
    "location_country": "Ardore, Calabria",
    "location_province": "Ardore, Reggio Calabria (RC)",
    "selected": true,
    "co2": "10,20"
  },
  {
    "fruit_id": 74882,
    "name": "mandarino tardivo di ciaculli",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595409710/fruits/biorfarm-icon-mandarino-tardivo-di-ciaculli_tree.png",
    "number": 150,
    "co2_unit": 0.06077,
    "farmer_name": "Fratelli Marceno",
    "location_country": " Palermo, Sicilia",
    "location_province": "Palermo (PA)",
    "selected": true,
    "co2": "9,12"
  },
  {
    "fruit_id": 114603,
    "name": "mela gala casarotti",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mela_gala_tree.png",
    "number": 150,
    "co2_unit": 0.04,
    "farmer_name": "Az. Agricola Famiglia Casarotti",
    "location_country": "Caldiero, Veneto",
    "location_province": "Caldiero, Verona (VR)",
    "selected": true,
    "co2": "6,00"
  },
  {
    "fruit_id": 78017,
    "name": "nocciola camponica sgusciata e tostata",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1599041778/fruits/biorfarm-icon-nocciola-camponica_tree.png",
    "number": 50,
    "co2_unit": 0.01799,
    "farmer_name": "Filippo Farinaro",
    "location_country": "Presenzano, Campania",
    "location_province": "Presenzano, Caserta (CE)",
    "selected": true,
    "co2": "0,90"
  },
  {
    "fruit_id": 11636,
    "name": "pera williams",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-pera_williams_tree.png",
    "number": 100,
    "co2_unit": 0.046,
    "farmer_name": "Fratelli Brero",
    "location_country": "Lagnasco, Piemonte",
    "location_province": "Lagnasco, Cuneo (CN)",
    "selected": true,
    "co2": "4,60"
  },
  {
    "fruit_id": 76304,
    "name": "pesca gialla toscana",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-pesca_gialla_tree.png",
    "number": 60,
    "co2_unit": 0.063,
    "farmer_name": "Famiglia Checcucci",
    "location_country": "Roccastrada, Toscana",
    "location_province": "Roccastrada, Grosseto (GR)",
    "selected": true,
    "co2": "3,78"
  },
  {
    "fruit_id": 17811,
    "name": "susina nera",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-susina_nera_tree.png",
    "number": 100,
    "co2_unit": 0.024,
    "farmer_name": "Daniele Bucci",
    "location_country": "Faenza, Emilia Romagna",
    "location_province": "Faenza, Ravenna (RA)",
    "selected": true,
    "co2": "2,40"
  },
  {
    "fruit_id": 139519,
    "name": "susina santa clara",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1623063552/fruits/biorfarm-icon-susina-santa-clara_tree.png",
    "number": 90,
    "co2_unit": 0.024,
    "farmer_name": "Famiglia Banchio",
    "location_country": "Saluzzo, Piemonte",
    "location_province": "Saluzzo, Cuneo (CN)",
    "selected": true,
    "co2": "2,16"
  },
  {
    "fruit_id": 137216,
    "name": "uva italia",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1591870312/fruits/biorfarm-icon-uva-bianca_tree.png",
    "number": 63,
    "co2_unit": 0.04,
    "farmer_name": "Famiglia Losito",
    "location_country": "Rutigliano, Puglia",
    "location_province": "Rutigliano, Bari (BA)",
    "selected": true,
    "co2": "2,52"
  },
  {
    "fruit_id": 145283,
    "name": "uva italia",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1621419544/fruits/biorfarm-icon-uva-italia_tree.png",
    "number": 87,
    "co2_unit": 0.04,
    "farmer_name": "Elena Cassisi",
    "location_country": "Mazzarrone, Sicilia",
    "location_province": "Mazzarrone, Catania (CT)",
    "selected": true,
    "co2": "3,48"
  }
]
