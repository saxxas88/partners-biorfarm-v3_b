CO2 totali: xxxx Kg

Agricoltori supportati: 1

Alberi in partnerships: 125

Codice Sconto: no

Page URL: https://www.biorfarm.com/guber/

Logo URL: https://res.cloudinary.com/biorfarm/image/upload/v1639396203/static/guber-logo.png

Banner URL: no

Slogan: ' "Quando mai è stato fatto il miele da una sola ape in un alveare?"
(Thomas Hood)

Guber Banca ringrazia i propri clienti e collaboratori per la preziosa e quotidiana cooperazione,
senza la quale non avremmo potuto raggiungere gli attuali risultati!
Per esprimervi la nostra gratitudine, abbiamo deciso di coinvolgervi nel nostro grande alveare,
a sostegno della Famiglia Nucci e della loro Azienda Agricola produttrice di miele biologico.'