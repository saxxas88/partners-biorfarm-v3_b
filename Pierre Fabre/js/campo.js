/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 74881,
    "name": "mandarino avana",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595409639/fruits/biorfarm-icon-mandarino-avana_tree.png",
    "number": 20,
    "co2_unit": 0.06077,
    "farmer_name": "Fratelli Marceno",
    "location_country": " Palermo, Sicilia",
    "location_province": "Palermo (PA)",
    "selected": true,
    "co2": "1,22"
  },
  {
    "fruit_id": 679,
    "name": "mela golden delicious",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mela_golden_tree.png",
    "number": 20,
    "co2_unit": 0.04,
    "farmer_name": "Paolo Rossi",
    "location_country": "Clès, Trentino",
    "location_province": "Val di Non, Trento (TN)",
    "selected": true,
    "co2": "0,80"
  }
]