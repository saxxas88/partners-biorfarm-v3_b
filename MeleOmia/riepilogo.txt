CO2 totali: 6.800 Kg

Agricoltori supportati: 2

Alberi in partnerships: 200

Codice Sconto: undefined

Logo URL: https://res.cloudinary.com/biorfarm/image/upload/v1595497847/static/omia-logo.png

Banner URL: https://res.cloudinary.com/biorfarm/image/upload/v1603120338/static/b2b-header-omia.png

Slogan: "A Natale scegli la natura per chi ami e per il pianeta.
Con l’acquisto di un Cofanetto Omia contribuisci a supportare Biorfarm nella piantumazione di 100 alberi di Mele Golden e 100 alberi di Olivo, aiutando Lorenzo e Mario, piccoli agricoltori italiani in difficoltà."