/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    fruit_id: 234528,
    name: "clementina calabrese",
    icon_tree:
      "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-clementina_tree.png",
    number: 38,
    co2_unit: 0.06,
    farmer_name: "Az. Agricola Paolo Maria Lamenza",
    location_country: "Corigliano-Rossano, Calabria",
    location_province: "Corigliano-Rossano, Cosenza (CS)",
    selected: true,
    co2: "2,28",
  },
];
