/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    fruit_id: 188931,
    name: "arancia sanguinello antico",
    icon_tree:
      "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-arancia-sanguinello_tree.png",
    number: 37,
    co2_unit: 0.07,
    farmer_name: "Ilaria Campisi",
    location_country: "Caulonia, Calabria",
    location_province: "Caulonia, Reggio Calabria (RC)",
    selected: true,
    co2: "2,59",
  },
  {
    fruit_id: 187829,
    name: "clementina monreale",
    icon_tree:
      "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-clementina_tree.png",
    number: 15,
    co2_unit: 0.06,
    farmer_name: "Salvatore Cracchiolo",
    location_country: "Alcamo, Sicilia",
    location_province: "Alcamo, Trapani (TP)",
    selected: true,
    co2: "0,90",
  },
  {
    fruit_id: 162646,
    name: "mandarino siciliano",
    icon_tree:
      "https://res.cloudinary.com/biorfarm/image/upload/v1638871720/fruits/biorfarm-icon-mandarino_tree.png",
    number: 32,
    co2_unit: 0.06077,
    farmer_name: "Azienda Agricola Parisi",
    location_country: "Lentini, Sicilia",
    location_province: "Lentini, Siracusa (SR)",
    selected: true,
    co2: "1,94",
  },
  {
    fruit_id: 74882,
    name: "mandarino tardivo di ciaculli",
    icon_tree:
      "https://res.cloudinary.com/biorfarm/image/upload/v1595409710/fruits/biorfarm-icon-mandarino-tardivo-di-ciaculli_tree.png",
    number: 70,
    co2_unit: 0.06077,
    farmer_name: "Fratelli Marceno",
    location_country: " Palermo, Sicilia",
    location_province: "Palermo (PA)",
    selected: true,
    co2: "4,25",
  },
  {
    fruit_id: 114606,
    name: "mela fujion",
    icon_tree:
      "https://res.cloudinary.com/biorfarm/image/upload/v1610988794/fruits/biorfarm-icon-mela_fujion_tree.png",
    number: 80,
    co2_unit: 0.04,
    farmer_name: "Az. Agricola Famiglia Casarotti",
    location_country: "Caldiero, Veneto",
    location_province: "Caldiero, Verona (VR)",
    selected: true,
    co2: "3,20",
  },
  {
    fruit_id: 71503,
    name: "pera abate",
    icon_tree:
      "https://res.cloudinary.com/biorfarm/image/upload/v1593095538/fruits/biorfarm-icon-pera-abate_tree.png",
    number: 17,
    co2_unit: 0.046,
    farmer_name: "Lorenzo Sacchetto",
    location_country: "Lagnasco, Piemonte",
    location_province: "Lagnasco, Cuneo (CN)",
    selected: true,
    co2: "0,78",
  },
  {
    fruit_id: 121753,
    name: "pompelmo rosa",
    icon_tree:
      "https://res.cloudinary.com/biorfarm/image/upload/v1615544540/fruits/biorfarm-icon-pompelmo-rosa_tree.png",
    number: 9,
    co2_unit: 0.0906,
    farmer_name: "Cooperativa sociale ONLUS Si Può Fare",
    location_country: "Noto, Sicilia",
    location_province: "Noto, Siracusa (SR)",
    selected: true,
    co2: "0,82",
  },
];
