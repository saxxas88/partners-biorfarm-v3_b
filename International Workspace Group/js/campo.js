/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 121532,
    "name": "arancia newhall",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1615482760/fruits/biorfarm-icon-arancia_newhall_tree.png",
    "number": 5,
    "co2_unit": 0.07,
    "farmer_name": "Bruno Messina",
    "location_country": "Rosarno, Calabria",
    "location_province": "Rosarno, Reggio Calabria (RC)",
    "selected": true,
    "co2": "0,35"
  },
  {
    "fruit_id": 75225,
    "name": "arancia tarocco sant'alfio",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595581977/fruits/biorfarm-icon-arancia-tarocco-sant-alfio_tree.png",
    "number": 5,
    "co2_unit": 0.07,
    "farmer_name": "Emanuele Calvo",
    "location_country": "Mineo, Sicilia",
    "location_province": "Mineo, Catania (CT)",
    "selected": true,
    "co2": "0,35"
  },
  {
    "fruit_id": 683,
    "name": "clementina calabrese",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-clementina_tree.png",
    "number": 5,
    "co2_unit": 0.06,
    "farmer_name": "Paolo De Falco",
    "location_country": "Corigliano-Rossano, Calabria",
    "location_province": "Piana di Sibari, Corigliano-Rossano (CS)",
    "selected": true,
    "co2": "0,30"
  },
  {
    "fruit_id": 155931,
    "name": "mandarino tardivo di ciaculli",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595409710/fruits/biorfarm-icon-mandarino-tardivo-di-ciaculli_tree.png",
    "number": 5,
    "co2_unit": 0.06077,
    "farmer_name": "Filippo Licari",
    "location_country": "Marsala, Sicilia",
    "location_province": "Marsala, Trapani (TP)",
    "selected": true,
    "co2": "0,30"
  }
]
