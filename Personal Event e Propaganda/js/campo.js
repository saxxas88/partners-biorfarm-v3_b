/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    fruit_id: 227470,
    name: "alveare mix",
    icon_tree:
      "https://res.cloudinary.com/biorfarm/image/upload/v1616692593/fruits/biorfarm-icon-alveare-mix-lo-piccolo_tree.png",
    number: 25,
    co2_unit: 0,
    farmer_name: "Fratelli Campagna",
    location_country: "Pomarico, Basilicata",
    location_province: "Pomarico, Matera (MT)",
    selected: true,
    co2: "0,00",
  },
  {
    fruit_id: 682,
    name: "arancia navelina",
    icon: "https://res.cloudinary.com/biorfarm/image/upload/v1561625041/fruits/biorfarm-icon-arancia_navelina_fruit.png",
    icon_tree:
      "https://res.cloudinary.com/biorfarm/image/upload/v1561477692/fruits/biorfarm-icon-arancia_navelina_tree.png",
    img_tree:
      "https://www.biorfarm.com/wp-content/uploads/2020/03/Arance-Navel-di-Sicilia.jpg",
    number: 18,
    co2_unit: 0.07,
    farmer_name: "Paolo De Falco",
    location_country: "Corigliano-Rossano, Calabria",
    location_province: "Piana di Sibari, Corigliano-Rossano (CS)",
    selected: true,
    co2: "1,26",
  },
  {
    fruit_id: 683,
    name: "clementina calabrese",
    icon_tree:
      "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-clementina_tree.png",
    number: 10,
    co2_unit: 0.06,
    farmer_name: "Paolo De Falco",
    location_country: "Corigliano-Rossano, Calabria",
    location_province: "Piana di Sibari, Corigliano-Rossano (CS)",
    selected: true,
    co2: "0,60",
  },
];
