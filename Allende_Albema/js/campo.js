/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees=[
  {
    "fruit_id": 126720,
    "name": "ciliegia pugliese",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1618395058/fruits/biorfarm-icon-ciliegia-pugliese-galante_tree.png",
    "number": 17,
    "co2_unit": 0.125,
    "farmer_name": "Famiglia Galante",
    "location_country": "Ginosa, Puglia",
    "location_province": "Ginosa, Taranto (TA)",
    "selected": true,
    "co2": "2,13"
  },
  {
    "fruit_id": 63770,
    "name": "mandorla sgusciata dell'alta murgia",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1586198673/fruits/biorfarm-icon-mandorla-sgusciata_tree.png",
    "number": 18,
    "co2_unit": 0.17003,
    "farmer_name": "Famiglia Lobascio",
    "location_country": "Minervino, Puglia",
    "location_province": "Minervino, Barletta-Andria-Trani (BT)",
    "selected": true,
    "co2": "3,06"
  },
  {
    "fruit_id": 28708,
    "name": "olivo coratina",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1594972020/fruits/biorfarm-icon-olivo-coratina_tree.png",
    "number": 32,
    "co2_unit": 0.3,
    "farmer_name": "Famiglia Lobascio",
    "location_country": "Minervino, Puglia",
    "location_province": "Minervino, Barletta-Andria-Trani (BT)",
    "selected": true,
    "co2": "9,60"
  },
  {
    "fruit_id": 137391,
    "name": "pera carmosina",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1621957796/fruits/biorfarm-icon-pera-carmosina_tree.png",
    "number": 5,
    "co2_unit": 0.046,
    "farmer_name": "Famiglia Galante",
    "location_country": "Ginosa, Puglia",
    "location_province": "Ginosa, Taranto (TA)",
    "selected": true,
    "co2": "0,23"
  }
 ]