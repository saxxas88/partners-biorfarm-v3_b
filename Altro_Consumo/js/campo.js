/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees=[
    {
      "fruit_id": 682,
      "number": 31,
      "co2": "2,2",
      "location_country": "Corigliano-Rossano, Calabria",
      "location_province": "Piana di Sibari, Corigliano-Rossano (CS)"
    },
    {
      "fruit_id": 683,
      "number": 31,
      "co2": "1,9",
      "location_country": "Corigliano-Rossano, Calabria",
      "location_province": "Piana di Sibari, Corigliano-Rossano (CS)"
    },
    {
      "fruit_id": 5047,
      "number": 31,
      "co2": "2,8",
      "location_country": "Piana del Sele, Eboli",
      "location_province": "Piana del Sele, Eboli (SA)"
    },
    {
      "fruit_id": 33671,
      "number": 31,
      "co2": "2,2",
      "location_country": "Rocca di Capri Leone, Sicilia",
      "location_province": "Rocca di Capri Leone (ME)"
    },
    {
      "fruit_id": 33672,
      "number": 31,
      "co2": "2,2",
      "location_country": "Rocca di Capri Leone, Sicilia",
      "location_province": "Rocca di Capri Leone (ME)"
    },
    {
      "fruit_id": 60416,
      "number": 31,
      "co2": "2,2",
      "location_country": "Grammichele, Sicilia",
      "location_province": "Grammichele, Catania (CT)"
    },
    {
      "fruit_id": 61687,
      "number": 31,
      "co2": "2,2",
      "location_country": "Burgio, Sicilia",
      "location_province": "Burgio, Agrigento (AG)"
    },
    {
      "fruit_id": 63770,
      "number": 31,
      "co2": "2,2",
      "location_country": "Minervino, Puglia",
      "location_province": "Minervino, Barletta-Andria-Trani (BT)"
    },
    {
      "fruit_id": 73222,
      "number": 31,
      "co2": "2,2",
      "location_country": "Palagianello, Puglia",
      "location_province": "Palagianello, Taranto (TA)"
    },
    {
      "fruit_id": 74882,
      "number": 31,
      "co2": "2,2",
      "location_country": " Palermo, Sicilia",
      "location_province": "Palermo (PA)"
    },
    {
      "fruit_id": 75226,
      "number": 31,
      "co2": "2,2",
      "location_country": "Mineo, Sicilia",
      "location_province": "Mineo, Catania (CT)"
    },
    {
      "fruit_id": 75225,
      "number": 31,
      "co2": "2,2",
      "location_country": "Mineo, Sicilia",
      "location_province": "Mineo, Catania (CT)"
    }
 ]