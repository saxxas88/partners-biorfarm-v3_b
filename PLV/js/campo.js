/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 75223,
    "name": "arancia ovale",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595581789/fruits/biorfarm-icon-arancia-ovale_tree.png",
    "number": 10,
    "co2_unit": 0.07,
    "farmer_name": "Emanuele Calvo",
    "location_country": "Mineo, Sicilia",
    "location_province": "Mineo, Catania (CT)",
    "selected": true,
    "co2": "0,70"
  },
  {
    "fruit_id": 102169,
    "name": "arancia valencia calvo",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477692/fruits/biorfarm-icon-arancia_valencia_tree.png",
    "number": 10,
    "co2_unit": 0.07,
    "farmer_name": "Emanuele Calvo",
    "location_country": "Mineo, Sicilia",
    "location_province": "Mineo, Catania (CT)",
    "selected": true,
    "co2": "0,70"
  }
]
