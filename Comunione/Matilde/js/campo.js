/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees=[
  {
    "fruit_id": 167651,
    "name": "arancia biondo della riviera dei gelsomini",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1639568082/fruits/biorfarm-icon-arancia-biondo_tree.png",
    "number": 1,
    "co2_unit": 0.07,
    "farmer_name": "Giovanni Zappavigna",
    "location_country": "Ardore, Calabria",
    "location_province": "Ardore, Reggio Calabria (RC)",
    "selected": true,
    "co2": "0,07"
  },
  {
    "fruit_id": 162058,
    "name": "arancia rossa di sicilia IGP",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1638871720/fruits/biorfarm-icon-arancia-rossa_tree.png",
    "number": 1,
    "co2_unit": 0.07,
    "farmer_name": "Azienda Agricola Parisi",
    "location_country": "Lentini, Sicilia",
    "location_province": "Lentini, Siracusa (SR)",
    "selected": true,
    "co2": "0,07"
  },
  {
    "fruit_id": 82556,
    "name": "baby kiwi",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1602753297/fruits/biorfarm-icon-baby_kiwi_tree.png",
    "number": 1,
    "co2_unit": 0.00115,
    "farmer_name": "Fabio Proverbio",
    "location_country": "Treviglio, Lombardia",
    "location_province": "Treviglio, Bergamo (BG)",
    "selected": true,
    "co2": "0,00"
  },
  {
    "fruit_id": 73222,
    "name": "fragola melissa puglia",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1591263270/fruits/biorfarm-icon-fragola_tree.png",
    "number": 1,
    "co2_unit": 0.00084,
    "farmer_name": "Famiglia Mansueto",
    "location_country": "Palagianello, Puglia",
    "location_province": "Palagianello, Taranto (TA)",
    "selected": true,
    "co2": "0,00"
  },
  {
    "fruit_id": 187519,
    "name": "kiwi boerica",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1581586234/fruits/biorfarm-icon-kiwi_tree.png",
    "number": 1,
    "co2_unit": 0.00115,
    "farmer_name": "Rocco Agostino",
    "location_country": "Anoia, Calabria",
    "location_province": "Anoia, Reggio Calabria (RC)",
    "selected": true,
    "co2": "0,00"
  },
  {
    "fruit_id": 681,
    "name": "mela fuji",
    "icon": "https://res.cloudinary.com/biorfarm/image/upload/v1561625042/fruits/biorfarm-icon-mela_fuji_fruit.png",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mela_fuji_tree.png",
    "img_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1500289952/fruits/mela_fuji_biorfarm_frutta.jpg",
    "number": 1,
    "co2_unit": 0.04,
    "farmer_name": "Paolo Rossi",
    "location_country": "Clès, Trentino",
    "location_province": "Val di Non, Clès (TN)",
    "selected": true,
    "co2": "0,04"
  },
  {
    "fruit_id": 127329,
    "name": "mela golden delicious",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mela_golden_tree.png",
    "number": 1,
    "co2_unit": 0.04,
    "farmer_name": "Mirko Bezzi",
    "location_country": "Urbana, Veneto",
    "location_province": "Urbana, Padova (PD)",
    "selected": true,
    "co2": "0,04"
  },
  {
    "fruit_id": 33357,
    "name": "melograno akko",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-melograno-akko_tree.png",
    "number": 1,
    "co2_unit": 0.05,
    "farmer_name": "Giuseppe Costantino",
    "location_country": "Alcamo, Sicilia",
    "location_province": "Alcamo, Trapani (TP)",
    "selected": true,
    "co2": "0,05"
  },
  {
    "fruit_id": 33356,
    "name": "melograno wonderful",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-melograno-wonderful_tree.png",
    "number": 1,
    "co2_unit": 0.05,
    "farmer_name": "Giuseppe Costantino",
    "location_country": "Alcamo, Sicilia",
    "location_province": "Alcamo, Trapani (TP)",
    "selected": true,
    "co2": "0,05"
  },
  {
    "fruit_id": 71503,
    "name": "pera abate",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1593095538/fruits/biorfarm-icon-pera-abate_tree.png",
    "number": 1,
    "co2_unit": 0.046,
    "farmer_name": "Lorenzo Sacchetto",
    "location_country": "Lagnasco, Piemonte",
    "location_province": "Lagnasco, Cuneo (CN)",
    "selected": true,
    "co2": "0,05"
  },
  {
    "fruit_id": 127283,
    "name": "pera santa maria",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1618823679/fruits/biorfarm-icon-pera-santa-maria_tree.png",
    "number": 1,
    "co2_unit": 0.046,
    "farmer_name": "Mirko Bezzi",
    "location_country": "Urbana, Veneto",
    "location_province": "Urbana, Padova (PD)",
    "selected": true,
    "co2": "0,05"
  },
  {
    "fruit_id": 139519,
    "name": "susina santa clara",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1623063552/fruits/biorfarm-icon-susina-santa-clara_tree.png",
    "number": 1,
    "co2_unit": 0.024,
    "farmer_name": "Famiglia Banchio",
    "location_country": "Saluzzo, Piemonte",
    "location_province": "Saluzzo, Cuneo (CN)",
    "selected": true,
    "co2": "0,02"
  },
  {
    "fruit_id": 78574,
    "name": "uva bianca moscato",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1599730568/fruits/biorfarm-icon-uva-bianca-moscato_tree.png",
    "number": 1,
    "co2_unit": 0.04,
    "farmer_name": "Fabio Proverbio",
    "location_country": "Treviglio, Lombardia",
    "location_province": "Treviglio, Bergamo (BG)",
    "selected": true,
    "co2": "0,04"
  },
  {
    "fruit_id": 70009,
    "name": "uva crimson rossa",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1591869855/fruits/biorfarm-icon-uva-crimson-rossa_tree.png",
    "number": 1,
    "co2_unit": 0.04,
    "farmer_name": "Famiglia Galante",
    "location_country": "Ginosa, Puglia",
    "location_province": "Ginosa, Taranto (TA)",
    "selected": true,
    "co2": "0,04"
  },
  {
      "fruit_id": 156432,
      "name": "mela granny smith",
      "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1586198673/fruits/biorfarm-icon-mela-granny-smith_tree.png",
      "number": 1,
      "co2_unit": 0.04,
      "farmer_name": "Diego Boggian",
      "location_country": "Legnago, Veneto",
      "location_province": "Legnago, Verona (VR)",
      "selected": true,
      "co2": "0,04"
    }
    ]