CO2 totali: 560 Kg

Agricoltori supportati: 15

Alberi in partnerships: 12

Codice Sconto: no

Page URL: https://www.biorfarm.com/comunionematilde/

Logo URL: https://res.cloudinary.com/biorfarm/image/upload/v1621961694/static/comunione-logo.png

Banner URL: https://res.cloudinary.com/biorfarm/image/upload/v1622022099/static/b2b-header-comunione.png

Slogan: Prima Comunione di Matilde 15 Ottobre 2022
"Ogni cosa che puoi immaginare, la natura l’ha già creata.
(Albert Einstein)
Grazie per essermi vicini in questo momento così importante e per realizzare insieme a me il Frutteto di Matilde!"