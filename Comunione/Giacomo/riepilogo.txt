CO2 totali: 13 Kg

Agricoltori supportati: 1

Alberi in partnerships: 15

Codice Sconto: coupon_code

Page URL: https://www.biorfarm.com/comunione-giacomo/

Logo URL: https://res.cloudinary.com/biorfarm/image/upload/v1621961694/static/comunione-logo.png

Banner URL: https://res.cloudinary.com/biorfarm/image/upload/v1622022099/static/b2b-header-comunione.png

Slogan: Prima Comunione di Giacomo 6 giugno 2021
"Due sono le ali per volare al cielo: la confessione e la comunione" cit. San Giovanni Bosco.
Grazie per aver condiviso con me questo momento così importante.
Giacomo"