/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 89488,
    "name": "alveare mix",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1606987616/fruits/biorfarm-icon-alveare-mix_tree.png",
    "number": 5,
    "co2_unit": 0,
    "farmer_name": "Famiglia Nucci",
    "location_country": "Santarcangelo di Romagna, Emilia-Romagna",
    "location_province": "Santarcangelo di Romagna, Rimini (RN)",
    "selected": true,
    "co2": "0,00"
  },
  {
    "fruit_id": 37211,
    "name": "castagna palummina",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-castagna_tree.png",
    "number": 5,
    "co2_unit": 1.589,
    "farmer_name": "Alfredo Di Cicilia",
    "location_country": "Montella, Campania",
    "location_province": "Montella, Avellino (AV)",
    "selected": true,
    "co2": "7,95"
  },
  {
    "fruit_id": 276159,
    "name": "filare aleatico - vino montesenano",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1612950789/fruits/filare-litiano-sangiovese-maremma-doc.png",
    "number": 5,
    "co2_unit": 0,
    "farmer_name": "Villa Caviciana",
    "location_country": "Gradoli, Lazio",
    "location_province": "Gradoli, Viterbo (VT)",
    "selected": true,
    "co2": "0,00"
  },
  {
    "fruit_id": 78574,
    "name": "uva bianca moscato",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1599730568/fruits/biorfarm-icon-uva-bianca-moscato_tree.png",
    "number": 5,
    "co2_unit": 0.04,
    "farmer_name": "Fabio Proverbio",
    "location_country": "Treviglio, Lombardia",
    "location_province": "Treviglio, Bergamo (BG)",
    "selected": true,
    "co2": "0,20"
  }
]
