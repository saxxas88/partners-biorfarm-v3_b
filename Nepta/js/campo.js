/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 129165,
    "name": "arancia tarocco",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-arancia-tarocco_tree.png",
    "number": 38,
    "co2_unit": 0.07,
    "farmer_name": "Angela Arcoria",
    "location_country": "Paternò, Sicilia",
    "location_province": "Paternò, Catania (CT)",
    "selected": true,
    "co2": "2,66"
  },
  {
    "fruit_id": 217764,
    "name": "ciliegia grace star",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477692/fruits/biorfarm-icon-ciliegia-ferrovia-tree-2.png",
    "number": 38,
    "co2_unit": 0.125,
    "farmer_name": "Luca Schiavulli",
    "location_country": "Casamassima, Puglia",
    "location_province": "Casamassima, Bari (BA)",
    "selected": true,
    "co2": "4,75"
  },
  {
    "fruit_id": 162646,
    "name": "mandarino siciliano",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1638871720/fruits/biorfarm-icon-mandarino_tree.png",
    "number": 38,
    "co2_unit": 0.06077,
    "farmer_name": "Azienda Agricola Parisi",
    "location_country": "Lentini, Sicilia",
    "location_province": "Lentini, Siracusa (SR)",
    "selected": true,
    "co2": "2,31"
  },
  {
    "fruit_id": 679,
    "name": "mela golden delicious",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mela_golden_tree.png",
    "number": 38,
    "co2_unit": 0.04,
    "farmer_name": "Paolo Rossi",
    "location_country": "Clès, Trentino",
    "location_province": "Val di Non, Trento (TN)",
    "selected": true,
    "co2": "1,52"
  },
  {
    "fruit_id": 230895,
    "name": "pesca nettarina romagna red",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1620376201/fruits/biorfarm-icon-nettarina-romagna-big_tree.png",
    "number": 38,
    "co2_unit": 0.089,
    "farmer_name": "Famiglia Scardovi",
    "location_country": "Faenza, Emilia Romagna",
    "location_province": "Faenza, Ravenna (RA)",
    "selected": true,
    "co2": "3,38"
  }
]