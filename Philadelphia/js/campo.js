/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    fruit_id: 156693,
    name: "mandorla a cuore dei monti nebrodi",
    icon_tree:
      "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mandorla_tree.png",
    number: 115,
    co2_unit: 0.17003,
    farmer_name: "Famiglia Virzì",
    location_country: "Cesarò, Sicilia",
    location_province: "Cesarò, Messina (ME)",
    selected: true,
    co2: "19,55",
  },
  {
    fruit_id: 14646,
    name: "mandorla di noto sgusciata",
    icon_tree:
      "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mandorla_tree.png",
    number: 124,
    co2_unit: 0.17003,
    farmer_name: "Famiglia Leone",
    location_country: "Noto, Sicilia",
    location_province: "Noto, Siracusa (SR)",
    selected: true,
    co2: "21,08",
  },
  {
    fruit_id: 238801,
    name: "mandorla sarda sgusciata",
    icon_tree:
      "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mandorla_tree.png",
    number: 123,
    co2_unit: 0.17003,
    farmer_name: "Paolo Masala",
    location_country: "Cagliari, Sardegna",
    location_province: "Cagliari (CA)",
    selected: true,
    co2: "20,91",
  },
  {
    fruit_id: 63770,
    name: "mandorla sgusciata dell'alta murgia",
    icon_tree:
      "https://res.cloudinary.com/biorfarm/image/upload/v1586198673/fruits/biorfarm-icon-mandorla-sgusciata_tree.png",
    number: 124,
    co2_unit: 0.17003,
    farmer_name: "Famiglia Lobascio",
    location_country: "Minervino, Puglia",
    location_province: "Minervino, Barletta-Andria-Trani (BT)",
    selected: true,
    co2: "21,08",
  },
];
