/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
    {
        "fruit_id": 685,
        "name": "olivo secolare dop bruzio",
        "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1594971956/fruits/biorfarm-icon-olivo-bruzio_tree.png",
        "number": 53,
        "co2_unit": 0.3,
        "farmer_name": "Guglielmo Converso",
        "location_country": "Colline pre-Silane, Calabria",
        "location_province": "Colline pre-Silane, Corigliano-Rossano (CS)",
        "selected": true,
        "co2": "15,90"
    },
    {
        "fruit_id": 28708,
        "name": "olivo coratina",
        "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1594972020/fruits/biorfarm-icon-olivo-coratina_tree.png",
        "number": 6,
        "co2_unit": 0.3,
        "farmer_name": "Famiglia Lobascio",
        "location_country": "Minervino, Puglia",
        "location_province": "Minervino, Barletta-Andria-Trani (BT)",
        "selected": true,
        "co2": "1,80"
    },
    {
        "fruit_id": 227470,
        "name": "alveare mix",
        "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1616692593/fruits/biorfarm-icon-alveare-mix-lo-piccolo_tree.png",
        "number": 176,
        "co2_unit": 0,
        "farmer_name": "Fratelli Campagna",
        "location_country": "Pomarico, Basilicata",
        "location_province": "Pomarico, Matera (MT)",
        "selected": true,
        "co2": "0,00"
    },
    {
        "fruit_id": 89679,
        "name": "alveare millefiori",
        "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1606987616/fruits/biorfarm-icon-alveare-millefiori_tree.png",
        "number": 88,
        "co2_unit": 0,
        "farmer_name": "Famiglia Nucci",
        "location_country": "Santarcangelo di Romagna, Emilia-Romagna",
        "location_province": "Santarcangelo di Romagna, Rimini (RN)",
        "selected": true,
        "co2": "0,00"
    }
]