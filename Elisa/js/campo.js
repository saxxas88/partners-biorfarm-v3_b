/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 12368,
    "name": "fico d'india rosso",
    "icon_tree": "https://s3.eu-central-1.amazonaws.com/biorfarm-eu-de/icons/14_fico_india_rosso_tree.png",
    "number": 50,
    "co2_unit": 0.085,
    "farmer_name": "Rosa Bruno",
    "location_country": "Barrafranca, Sicilia",
    "location_province": "Barrafranca, Enna (EN)",
    "selected": true,
    "co2": "4,25"
  },
  {
    "fruit_id": 205002,
    "name": "mela gala",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mela_gala_tree.png",
    "number": 25,
    "co2_unit": 0.04,
    "farmer_name": "Lorenzo Sacchetto",
    "location_country": "Lagnasco, Piemonte",
    "location_province": "Lagnasco, Cuneo (CN)",
    "selected": true,
    "co2": "1,00"
  },
  {
    "fruit_id": 127213,
    "name": "pera williams",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-pera_williams_tree.png",
    "number": 25,
    "co2_unit": 0.046,
    "farmer_name": "Mirko Bezzi",
    "location_country": "Urbana, Veneto",
    "location_province": "Urbana, Padova (PD)",
    "selected": true,
    "co2": "1,15"
  }
]
