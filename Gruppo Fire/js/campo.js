/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees=[
  {
    "fruit_id": 129165,
    "name": "arancia tarocco",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-arancia-tarocco_tree.png",
    "number": 50,
    "co2_unit": 0.07,
    "farmer_name": "Angela Arcoria",
    "location_country": "Paternò, Sicilia",
    "location_province": "Paternò, Catania (CT)",
    "selected": true,
    "co2": "3,50"
  },
  {
    "fruit_id": 121753,
    "name": "pompelmo rosa",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1615544540/fruits/biorfarm-icon-pompelmo-rosa_tree.png",
    "number": 50,
    "co2_unit": 0.0906,
    "farmer_name": "Cooperativa sociale ONLUS Si Può Fare",
    "location_country": "Noto, Sicilia",
    "location_province": "Noto, Siracusa (SR)",
    "selected": true,
    "co2": "4,53"
  }
]