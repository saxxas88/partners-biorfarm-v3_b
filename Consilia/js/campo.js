/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 156022,
    "name": "alveare millefiori",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1607338355/fruits/biorfarm-icon-alveare-millefiori_tree.png",
    "number": 16,
    "co2_unit": 0,
    "farmer_name": "Stefano De Pascale",
    "location_country": "Pisa, Toscana",
    "location_province": "Pisa (PI)",
    "selected": true,
    "co2": "0,00"
  },
  {
    "fruit_id": 75225,
    "name": "arancia tarocco sant'alfio",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595581977/fruits/biorfarm-icon-arancia-tarocco-sant-alfio_tree.png",
    "number": 19,
    "co2_unit": 0.07,
    "farmer_name": "Emanuele Calvo",
    "location_country": "Mineo, Sicilia",
    "location_province": "Mineo, Catania (CT)",
    "selected": true,
    "co2": "1,33"
  },
  {
    "fruit_id": 683,
    "name": "clementina calabrese",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-clementina_tree.png",
    "number": 19,
    "co2_unit": 0.06,
    "farmer_name": "Paolo De Falco",
    "location_country": "Corigliano-Rossano, Calabria",
    "location_province": "Piana di Sibari, Corigliano-Rossano (CS)",
    "selected": true,
    "co2": "1,14"
  },
  {
    "fruit_id": 166373,
    "name": "fragola veneta",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1591263270/fruits/biorfarm-icon-fragola_tree.png",
    "number": 12,
    "co2_unit": 0.00011,
    "farmer_name": "Azienda Agricola Fiorio",
    "location_country": "Roverchiara, Veneto",
    "location_province": "Roverchiara, Verona (VR)",
    "selected": true,
    "co2": "0,001"
  },
  {
    "fruit_id": 61900,
    "name": "mirtillo duke",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1584521826/fruits/biorfarm-icon-mirtillo-duke_tree.png",
    "number": 12,
    "co2_unit": 0.00084,
    "farmer_name": "Azienda Agricola Magna Rosa",
    "location_country": "Barge, Piemonte",
    "location_province": "Barge, Cuneo (CN)",
    "selected": true,
    "co2": "0,01"
  },
  {
    "fruit_id": 70580,
    "name": "olivo igp toscano",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1594972031/fruits/biorfarm-icon-olivo-igp-toscano_tree.png",
    "number": 6,
    "co2_unit": 0.3,
    "farmer_name": "Famiglia Checcucci",
    "location_country": "Roccastrada, Toscana",
    "location_province": "Roccastrada, Grosseto (GR)",
    "selected": true,
    "co2": "1,80"
  }
]