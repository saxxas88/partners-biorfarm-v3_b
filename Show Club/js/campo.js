/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 75223,
    "name": "arancia ovale",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595581789/fruits/biorfarm-icon-arancia-ovale_tree.png",
    "number": 20,
    "co2_unit": 0.07,
    "farmer_name": "Emanuele Calvo",
    "location_country": "Mineo, Sicilia",
    "location_province": "Mineo, Catania (CT)",
    "selected": true,
    "co2": "1,40"
  },
  {
    "fruit_id": 60416,
    "name": "arancia tarocco",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-arancia-tarocco_tree.png",
    "number": 20,
    "co2_unit": 0.07,
    "farmer_name": "Giuseppe Vanella",
    "location_country": "Grammichele, Sicilia",
    "location_province": "Grammichele, Catania (CT)",
    "selected": true,
    "co2": "1,40"
  },
  {
    "fruit_id": 683,
    "name": "clementina calabrese",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-clementina_tree.png",
    "number": 20,
    "co2_unit": 0.06,
    "farmer_name": "Paolo De Falco",
    "location_country": "Corigliano-Rossano, Calabria",
    "location_province": "Piana di Sibari, Corigliano-Rossano (CS)",
    "selected": true,
    "co2": "1,20"
  },
  {
    "fruit_id": 61443,
    "name": "kiwi boerica",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1581586234/fruits/biorfarm-icon-kiwi_tree.png",
    "number": 20,
    "co2_unit": 0.00115,
    "farmer_name": "Azienda Agricola Cordopatri",
    "location_country": "Taurianova, Calabria",
    "location_province": "Taurianova, Reggio Calabria (RC)",
    "selected": true,
    "co2": "0,02"
  },
  {
    "fruit_id": 127329,
    "name": "mela golden delicious",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mela_golden_tree.png",
    "number": 20,
    "co2_unit": 0.04,
    "farmer_name": "Mirko Bezzi",
    "location_country": "Urbana, Veneto",
    "location_province": "Urbana, Padova (PD)",
    "selected": true,
    "co2": "0,80"
  }
]
