/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees=[
  {
    "fruit_id": 679,
    "name": "mela golden delicious",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mela_golden_tree.png",
    "number": 115,
    "co2_unit": 0.04,
    "farmer_name": "Paolo Rossi",
    "location_country": "Clès, Trentino",
    "location_province": "Val di Non, Trento (TN)",
    "selected": true,
    "co2": "4,60"
  },
  {
    "fruit_id": 632,
    "name": "mela renetta del canada",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mela_renetta_tree.png",
    "number": 115,
    "co2_unit": 0.04,
    "farmer_name": "Paolo Rossi",
    "location_country": "Clès, Trentino",
    "location_province": "Val di Non, Trento (TN)",
    "selected": true,
    "co2": "4,60"
  },
  {
    "fruit_id": 71503,
    "name": "pera abate",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1593095538/fruits/biorfarm-icon-pera-abate_tree.png",
    "number": 50,
    "co2_unit": 0.046,
    "farmer_name": "Lorenzo Sacchetto",
    "location_country": "Lagnasco, Piemonte",
    "location_province": "Lagnasco, Cuneo (CN)",
    "selected": true,
    "co2": "2,30"
  },
  {
    "fruit_id": 127283,
    "name": "pera santa maria",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1618823679/fruits/biorfarm-icon-pera-santa-maria_tree.png",
    "number": 50,
    "co2_unit": 0.046,
    "farmer_name": "Mirko Bezzi",
    "location_country": "Urbana, Veneto",
    "location_province": "Urbana, Padova (PD)",
    "selected": true,
    "co2": "2,30"
  },
  {
    "fruit_id": 11636,
    "name": "pera williams",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-pera_williams_tree.png",
    "number": 100,
    "co2_unit": 0.046,
    "farmer_name": "Fratelli Brero",
    "location_country": "Lagnasco, Piemonte",
    "location_province": "Lagnasco, Cuneo (CN)",
    "selected": true,
    "co2": "4,60"
  }
]