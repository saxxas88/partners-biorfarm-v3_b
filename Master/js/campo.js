/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 32881,
    "name": "albicocca rossa",
    "icon": "https://res.cloudinary.com/biorfarm/image/upload/v1561625041/fruits/biorfarm-icon-albicocca_rossa_fruit.png",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-albicocca-rossa_tree.png",
    "img_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1562324133/fruits/albicocca_rossa_biorfarm_frutta.jpg",
    "number": 20,
    "co2_unit": 0.07,
    "farmer_name": "Antonio Giannella",
    "location_country": "San Ferdinando, Puglia",
    "location_province": "San Ferdinando, Barletta-Andria-Trani (BT)",
    "selected": true,
    "co2": "1,40"
  },
  {
    "fruit_id": 123917,
    "name": "arancia moro",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-arancia-moro_tree.png",
    "number": 20,
    "co2_unit": 0.07,
    "farmer_name": "Carlo Marilli",
    "location_country": "Carlentini, Sicilia",
    "location_province": "Carlentini, Siracusa (SR)",
    "selected": true,
    "co2": "1,40"
  },
  {
    "fruit_id": 113208,
    "name": "clementina tardiva",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1610451045/fruits/biorfarm-icon-clementina-tardiva_tree.png",
    "number": 20,
    "co2_unit": 0.065,
    "farmer_name": "Az. Agricola Paolo Maria Lamenza",
    "location_country": "Corigliano-Rossano, Calabria",
    "location_province": "Corigliano-Rossano, Cosenza (CS)",
    "selected": true,
    "co2": "1,30"
  },
  {
    "fruit_id": 136868,
    "name": "mela annurca campana",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1620924659/fruits/biorfarm-icon-mela-annurca-campana_tree.png",
    "number": 20,
    "co2_unit": 0.04,
    "farmer_name": "Castrese Galluccio",
    "location_country": "Giugliano in Campania, Campania",
    "location_province": "Giugliano in Campania, Napoli (NA)",
    "selected": true,
    "co2": "0,80"
  },
  {
    "fruit_id": 137216,
    "name": "uva italia",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1591870312/fruits/biorfarm-icon-uva-bianca_tree.png",
    "number": 20,
    "co2_unit": 0.04,
    "farmer_name": "Famiglia Losito",
    "location_country": "Rutigliano, Puglia",
    "location_province": "Rutigliano, Bari (BA)",
    "selected": true,
    "co2": "0,80"
  }
]
