CO2 totali: 17.500 Kg

Agricoltori supportati: 1

Alberi in partnerships: 250

Codice Sconto: no

Page URL: https://www.biorfarm.com/salonecsr2022/

Logo URL: https://res.cloudinary.com/biorfarm/image/upload/v1658249523/static/CSR-10_anni-logo.svg

Banner URL: 

Slogan: "In occasione della 10° edizione del Salone della CSR e dell’innovazione sociale nasce il Frutteto del Salone della CSR. I relatori dell’evento potranno adottare un albero e ricevere i suoi frutti.
Un gesto per ridurre l’inquinamento ambientale e sostenere i piccoli produttori locali in linea con lo spirito del Salone, uno spazio di approfondimento, condivisione e networking i cui frutti possono portare l’ispirazione e le connessioni necessarie al raggiungimento di obiettivi sempre più ambiziosi."