/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 60416,
    "name": "arancia tarocco",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-arancia-tarocco_tree.png",
    "number": 250,
    "co2_unit": 0.07,
    "farmer_name": "Giuseppe Vanella",
    "location_country": "Grammichele, Sicilia",
    "location_province": "Grammichele, Catania (CT)",
    "selected": true,
    "co2": "17,50"
  }
]