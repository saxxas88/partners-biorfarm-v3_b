/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 75223,
    "name": "arancia ovale",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595581789/fruits/biorfarm-icon-arancia-ovale_tree.png",
    "number": 50,
    "co2_unit": 0.07,
    "farmer_name": "Emanuele Calvo",
    "location_country": "Mineo, Sicilia",
    "location_province": "Mineo, Catania (CT)",
    "selected": true,
    "co2": "3,50"
  },
  {
    "fruit_id": 14648,
    "name": "arancia valencia",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477692/fruits/biorfarm-icon-arancia_valencia_tree.png",
    "number": 73,
    "co2_unit": 0.07,
    "farmer_name": "Famiglia Leone",
    "location_country": "Noto, Sicilia",
    "location_province": "Noto, Siracusa (SR)",
    "selected": true,
    "co2": "5,11"
  },
  {
    "fruit_id": 73291,
    "name": "ciliegia pugliese",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1592232023/fruits/biorfarm-icon-ciliegia-pugliese_tree.png",
    "number": 25,
    "co2_unit": 0.125,
    "farmer_name": "Fratelli Pavan",
    "location_country": "Castel del Monte, Puglia",
    "location_province": "Castel del Monte, Barletta-Andria-Trani (BT)",
    "selected": true,
    "co2": "3,13"
  },
  {
    "fruit_id": 73222,
    "name": "fragola melissa puglia",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1591263270/fruits/biorfarm-icon-fragola_tree.png",
    "number": 25,
    "co2_unit": 0.00084,
    "farmer_name": "Famiglia Mansueto",
    "location_country": "Palagianello, Puglia",
    "location_province": "Palagianello, Taranto (TA)",
    "selected": true,
    "co2": "0,02"
  },
  {
    "fruit_id": 156432,
    "name": "mela granny smith",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1586198673/fruits/biorfarm-icon-mela-granny-smith_tree.png",
    "number": 75,
    "co2_unit": 0.04,
    "farmer_name": "Diego Boggian",
    "location_country": "Legnago, Veneto",
    "location_province": "Legnago, Verona (VR)",
    "selected": true,
    "co2": "3,00"
  },
  {
    "fruit_id": 145283,
    "name": "uva italia",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1621419544/fruits/biorfarm-icon-uva-italia_tree.png",
    "number": 75,
    "co2_unit": 0.04,
    "farmer_name": "Elena Cassisi",
    "location_country": "Mazzarrone, Sicilia",
    "location_province": "Mazzarrone, Catania (CT)",
    "selected": true,
    "co2": "3,00"
  }
]