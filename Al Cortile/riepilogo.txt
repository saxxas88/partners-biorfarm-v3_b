CO2 totali: 674 Kg

Agricoltori supportati: 10

Alberi in partnerships: 13

Codice Sconto: ALCORTILE10

FORM : NO

Page URL: https://www.biorfarm.com/alcortile/

Logo URL: https://res.cloudinary.com/biorfarm/image/upload/v1685694925/static/al_cortile-logo.png

Banner URL: 

Slogan: "Al Cortile è attento alla sostenibilità ambientale e sociale del nostro territorio. Grazie a Biorfarm sosteniamo i piccoli agricoltori biologici italiani, protettori della biodiversità locale tramite forniture settimanali di frutta biologica utilizzata nelle portate del nostro menù."