/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 72045,
    "name": "nocciola tonda gentile igp in guscio",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595002902/fruits/biorfarm-icon-nocciola-tonda-gentile-igp_tree.png",
    "number": 70,
    "co2_unit": 0.01799,
    "farmer_name": "Naturalmente",
    "location_country": "Murazzano, Piemonte",
    "location_province": "Murazzano, Cuneo (CN)",
    "selected": true,
    "co2": "1,26"
  }
];
