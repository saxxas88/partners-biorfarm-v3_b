/****** CO2******/
//  newCO2 = (newNumber * co2) / number
/*************/
const partner_trees = [{
    //arancia navelina
        "fruit_id": 682,
        "number": 160,
        "co2": "11,2",
        "location_country": "Rossano, Calabria",
        "location_province": "Piana di Sibari, Rossano (CS)"
    },
    {
    //mela fuji
    "fruit_id": 681,
    "number": 100,
    "co2": "4,1",
    "location_country": "Clès, Trentino",
    "location_province": "Val di Non, Clès (TN)"
    },
    {
        //clementica calabrese
        "fruit_id": 683,
        "number": 170,
        "co2": "11",
        "location_country": "Rossano, Calabria",
        "location_province": "Piana di Sibari, Rossano (CS)"
    },
    {
        //pera williams
        "fruit_id": 11636,
        "number": 120,
        "co2": "6,6",
        "location_country": "Lagnasco, Piemonte",
        "location_province": "Lagnasco, Cuneo (CN)"
    },
    {
        //pesca gialla
        "fruit_id": 11701,
        "number": 100,
        "co2": "8,9",
        "location_country": "Lagnasco, Piemonte",
        "location_province": "Lagnasco, Cuneo (CN)"
    },
    {
        //albicocca pellecchiella
        "fruit_id": 5047,
        "number": 90,
        "co2": "6,3",
        "location_country": "Piana del Sele, Eboli",
        "location_province": "Piana del Sele, Eboli (SA)"
    },
    {
        //susina nera
        "fruit_id": 17811,
        "number": 97,
        "co2": "6,9",
        "location_country": "Faenza, Emilia Romagna",
        "location_province": "Faenza (RA)"
    },
    {
        //nettarina di faenza
        "fruit_id": 17812,
        "number": 60,
        "co2": "5,3",
        "location_country": "Faenza, Emilia Romagna",
        "location_province": "Faenza (RA)"
    },
    {
        //bergamotto di calabria
        "fruit_id": 17718,
        "number": 40,
        "co2": "2,7",
        "location_country": "Costa dei Gelsomini, Calabria",
        "location_province": "Ardore (RC)"
    },
    {
        //mandorla di noto
        "fruit_id": 14646,
        "number": 80,
        "co2": "2,5",
        "location_country": "Noto, Sicilia",
        "location_province": "Noto (SR)"
    },
    {
        //mela gala
        "fruit_id": 11526,
        "number": 90,
        "co2": "4,2",
        "location_country": "Lagnasco, Piemonte",
        "location_province": "Cuneo (CN)"
    }
];