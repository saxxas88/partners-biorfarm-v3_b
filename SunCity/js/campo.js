/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 60416,
    "name": "arancia tarocco",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-arancia-tarocco_tree.png",
    "number": 55,
    "co2_unit": 0.07,
    "farmer_name": "Giuseppe Vanella",
    "location_country": "Grammichele, Sicilia",
    "location_province": "Grammichele, Catania (CT)",
    "selected": true,
    "co2": "3,85"
  },
  {
    "fruit_id": 61687,
    "name": "arancia washington navel",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1584093722/fruits/biorfarm-icon-arancia-washington-navel_tree.png",
    "number": 55,
    "co2_unit": 0.07,
    "farmer_name": "Azienda Agricola Mirabella",
    "location_country": "Burgio, Sicilia",
    "location_province": "Burgio, Agrigento (AG)",
    "selected": true,
    "co2": "3,85"
  },
  {
    "fruit_id": 167874,
    "name": "mandarino della riviera dei gelsomini",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1638871720/fruits/biorfarm-icon-mandarino_tree.png",
    "number": 70,
    "co2_unit": 0.06077,
    "farmer_name": "Giovanni Zappavigna",
    "location_country": "Ardore, Calabria",
    "location_province": "Ardore, Reggio Calabria (RC)",
    "selected": true,
    "co2": "4,25"
  },
  {
    "fruit_id": 74882,
    "name": "mandarino tardivo di ciaculli",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595409710/fruits/biorfarm-icon-mandarino-tardivo-di-ciaculli_tree.png",
    "number": 55,
    "co2_unit": 0.06077,
    "farmer_name": "Fratelli Marceno",
    "location_country": " Palermo, Sicilia",
    "location_province": "Palermo (PA)",
    "selected": true,
    "co2": "3,34"
  }
]