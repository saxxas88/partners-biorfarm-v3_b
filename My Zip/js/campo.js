/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 75554,
    "name": "arancia biondo di caulonia",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595943654/fruits/biorfarm-icon-arancia-biondo-di-caulonia_tree.png",
    "number": 10,
    "co2_unit": 0.07,
    "farmer_name": "Ilaria Campisi",
    "location_country": "Caulonia, Calabria",
    "location_province": "Caulonia, Reggio Calabria (RC)",
    "selected": true,
    "co2": "0,70"
  },
  {
    "fruit_id": 102169,
    "name": "arancia valencia calvo",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477692/fruits/biorfarm-icon-arancia_valencia_tree.png",
    "number": 5,
    "co2_unit": 0.07,
    "farmer_name": "Emanuele Calvo",
    "location_country": "Mineo, Sicilia",
    "location_province": "Mineo, Catania (CT)",
    "selected": true,
    "co2": "0,35"
  },
  {
    "fruit_id": 60808,
    "name": "mela crimson",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1583232953/fruits/biorfarm-icon-mela-crimson_tree.png",
    "number": 5,
    "co2_unit": 0.04,
    "farmer_name": "Famiglia Fraire",
    "location_country": "Barge, Piemonte",
    "location_province": "Barge, Cuneo (CN)",
    "selected": true,
    "co2": "0,20"
  },
  {
    "fruit_id": 114603,
    "name": "mela gala casarotti",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mela_gala_tree.png",
    "number": 10,
    "co2_unit": 0.04,
    "farmer_name": "Az. Agricola Famiglia Casarotti",
    "location_country": "Caldiero, Veneto",
    "location_province": "Caldiero, Verona (VR)",
    "selected": true,
    "co2": "0,40"
  }
];
