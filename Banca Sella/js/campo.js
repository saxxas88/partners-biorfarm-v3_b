/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 75223,
    "name": "arancia ovale",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595581789/fruits/biorfarm-icon-arancia-ovale_tree.png",
    "number": 50,
    "co2_unit": 0.07,
    "farmer_name": "Emanuele Calvo",
    "location_country": "Mineo, Sicilia",
    "location_province": "Mineo, Catania (CT)",
    "selected": true,
    "co2": "3,50"
  },
  {
    "fruit_id": 102377,
    "name": "arancia tarocco gallo",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1608220860/fruits/biorfarm-icon-arancia-tarocco-gallo_tree.png",
    "number": 200,
    "co2_unit": 0.07,
    "farmer_name": "Emanuele Calvo",
    "location_country": "Mineo, Sicilia",
    "location_province": "Mineo, Catania (CT)",
    "selected": true,
    "co2": "14,00"
  },
  {
    "fruit_id": 102169,
    "name": "arancia valencia calvo",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477692/fruits/biorfarm-icon-arancia_valencia_tree.png",
    "number": 250,
    "co2_unit": 0.07,
    "farmer_name": "Emanuele Calvo",
    "location_country": "Mineo, Sicilia",
    "location_province": "Mineo, Catania (CT)",
    "selected": true,
    "co2": "17,50"
  },
  {
    "fruit_id": 17718,
    "name": "bergamotto di calabria",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477692/fruits/biorfarm-icon-bergamotto_tree.png",
    "number": 250,
    "co2_unit": 0.0477,
    "farmer_name": "Giovanni Zappavigna",
    "location_country": "Ardore, Calabria",
    "location_province": "Ardore, Reggio Calabria (RC)",
    "selected": true,
    "co2": "11,93"
  },
  {
    "fruit_id": 683,
    "name": "clementina calabrese",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-clementina_tree.png",
    "number": 300,
    "co2_unit": 0.06,
    "farmer_name": "Paolo De Falco",
    "location_country": "Corigliano-Rossano, Calabria",
    "location_province": "Piana di Sibari, Corigliano-Rossano (CS)",
    "selected": true,
    "co2": "18,00"
  },
  {
    "fruit_id": 38775,
    "name": "limone di calabria",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-limone-zagara-bianca_tree.png",
    "number": 300,
    "co2_unit": 0.068,
    "farmer_name": "Giovanni Zappavigna",
    "location_country": "Ardore, Calabria",
    "location_province": "Ardore, Reggio Calabria (RC)",
    "selected": true,
    "co2": "20,40"
  },
  {
    "fruit_id": 33671,
    "name": "limone zagara bianca",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-limone-zagara-bianca_tree.png",
    "number": 150,
    "co2_unit": 0.068,
    "farmer_name": "Fratelli Cupane",
    "location_country": "Rocca di Capri Leone, Sicilia",
    "location_province": "Rocca di Capri Leone, Messina (ME)",
    "selected": true,
    "co2": "10,20"
  },
  {
    "fruit_id": 74882,
    "name": "mandarino tardivo di ciaculli",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595409710/fruits/biorfarm-icon-mandarino-tardivo-di-ciaculli_tree.png",
    "number": 280,
    "co2_unit": 0.06077,
    "farmer_name": "Fratelli Marceno",
    "location_country": " Palermo, Sicilia",
    "location_province": "Palermo (PA)",
    "selected": true,
    "co2": "17,02"
  },
  {
    "fruit_id": 114603,
    "name": "mela gala casarotti",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mela_gala_tree.png",
    "number": 50,
    "co2_unit": 0.04,
    "farmer_name": "Az. Agricola Famiglia Casarotti",
    "location_country": "Caldiero, Veneto",
    "location_province": "Caldiero, Verona (VR)",
    "selected": true,
    "co2": "2,00"
  },
  {
    "fruit_id": 69905,
    "name": "pera coscia",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1591799301/fruits/biorfarm-icon-pera-coscia_tree.png",
    "number": 270,
    "co2_unit": 0.046,
    "farmer_name": "Famiglia Virzi",
    "location_country": "Cesarò, Sicilia",
    "location_province": "Cesarò, Messina (ME)",
    "selected": true,
    "co2": "12,42"
  },
  {
    "fruit_id": 11636,
    "name": "pera williams",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-pera_williams_tree.png",
    "number": 300,
    "co2_unit": 0.046,
    "farmer_name": "Fratelli Brero",
    "location_country": "Lagnasco, Piemonte",
    "location_province": "Lagnasco, Cuneo (CN)",
    "selected": true,
    "co2": "13,80"
  },
  {
    "fruit_id": 139519,
    "name": "susina santa clara",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1623063552/fruits/biorfarm-icon-susina-santa-clara_tree.png",
    "number": 50,
    "co2_unit": 0.024,
    "farmer_name": "Famiglia Banchio",
    "location_country": "Saluzzo, Piemonte",
    "location_province": "Saluzzo, Cuneo (CN)",
    "selected": true,
    "co2": "1,20"
  },
  {
    "fruit_id": 137216,
    "name": "uva italia",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1591870312/fruits/biorfarm-icon-uva-bianca_tree.png",
    "number": 50,
    "co2_unit": 0.04,
    "farmer_name": "Famiglia Losito",
    "location_country": "Rutigliano, Puglia",
    "location_province": "Rutigliano, Bari (BA)",
    "selected": true,
    "co2": "2,00"
  }
]