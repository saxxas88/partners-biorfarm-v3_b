CO2 totali: 15.120 Kg

Agricoltori supportati: 11

Alberi in partnerships: 250

Codice Sconto: no

Page URL: https://www.biorfarm.com/giffoni/

Logo URL: https://res.cloudinary.com/biorfarm/image/upload/v1655215844/Logo_Giffoni_vfm7ez.svg

Banner URL: undefined

Slogan: "A Giffoni investiamo sul benessere sociale e culturale delle future generazioni da più di mezzo secolo, stiamo operando ogni giorno per sensibilizzare le comunità future al rispetto e alla salvaguardia dell'ambiente. Vogliamo inviare un ulteriore messaggio di sostenibilità sociale e culturale e l'abbiamo fatto attraverso il Frutteto di Giffoni, in collaborazione con Biorfarm, la prima comunità agricola digitale d'Europa. Abbiamo adottato 250 alberi da frutto, sostenendo 11 piccoli agricoltori in difficoltà, abbiamo donato più di 1 quintale di frutta a enti no-profit del territorio campano e regalato l'iniziativa "Diventa Agricoltore per un giorno" ai ragazzi dell'Associazione Spes Unica, dando la possibilità di vivere un'esperienza formativa presso un'azienda agricola campana della rete Biorfarm. Questo è il nostro gesto dedicato alle future generazioni, come simbolo di speranza e di lieto avvenire."