/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 89679,
    "name": "alveare millefiori",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1606987616/fruits/biorfarm-icon-alveare-millefiori_tree.png",
    "number": 159,
    "co2_unit": 0,
    "farmer_name": "Famiglia Nucci",
    "location_country": "Santarcangelo di Romagna, Emilia-Romagna",
    "location_province": "Santarcangelo di Romagna, Rimini (RN)",
    "selected": true,
    "co2": "0,00"
  },
  {
    "fruit_id": 74882,
    "name": "mandarino tardivo di ciaculli",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595409710/fruits/biorfarm-icon-mandarino-tardivo-di-ciaculli_tree.png",
    "number": 180,
    "co2_unit": 0.06077,
    "farmer_name": "Fratelli Marceno",
    "location_country": " Palermo, Sicilia",
    "location_province": "Palermo (PA)",
    "selected": true,
    "co2": "10,94"
  },
  {
    "fruit_id": 260810,
    "name": "noce lara",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1622821393/fruits/biorfarm-icon-noce-essiccata_tree.png",
    "number": 30,
    "co2_unit": 0.83943,
    "farmer_name": "Claudio Granelli",
    "location_country": "Varano de Melegari, Emilia-Romagna",
    "location_province": "Varano de Melegari, Parma (PR)",
    "selected": true,
    "co2": "25,18"
  }
]