/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 258495,
    "name": "mirtillo aido",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1684507391/fruits/biorfarm-icon-mirtillo-aido_tree.png",
    "number": 25,
    "co2_unit": 0.00084,
    "farmer_name": "Famiglia Fraire",
    "location_country": "Barge, Piemonte",
    "location_province": "Barge, Cuneo (CN)",
    "selected": true,
    "co2": "0,02"
  }
]