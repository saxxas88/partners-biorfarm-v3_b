/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 89695,
    "name": "alveare castagno",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1606987616/fruits/biorfarm-icon-alveare-castagno_tree.png",
    "number": 150,
    "co2_unit": 0,
    "farmer_name": "Famiglia Nucci",
    "location_country": "Santarcangelo di Romagna, Emilia-Romagna",
    "location_province": "Santarcangelo di Romagna, Rimini (RN)",
    "selected": true,
    "co2": "0,00"
  },
  {
    "fruit_id": 89679,
    "name": "alveare millefiori",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1606987616/fruits/biorfarm-icon-alveare-millefiori_tree.png",
    "number": 150,
    "co2_unit": 0,
    "farmer_name": "Famiglia Nucci",
    "location_country": "Santarcangelo di Romagna, Emilia-Romagna",
    "location_province": "Santarcangelo di Romagna, Rimini (RN)",
    "selected": true,
    "co2": "0,00"
  }
]