/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 167651,
    "name": "arancia biondo della riviera dei gelsomini",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1639568082/fruits/biorfarm-icon-arancia-biondo_tree.png",
    "number": 20,
    "co2_unit": 0.07,
    "farmer_name": "Giovanni Zappavigna",
    "location_country": "Ardore, Calabria",
    "location_province": "Ardore, Reggio Calabria (RC)",
    "selected": true,
    "co2": "1,40"
  },
  {
    "fruit_id": 60416,
    "name": "arancia tarocco",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-arancia-tarocco_tree.png",
    "number": 20,
    "co2_unit": 0.07,
    "farmer_name": "Giuseppe Vanella",
    "location_country": "Grammichele, Sicilia",
    "location_province": "Grammichele, Catania (CT)",
    "selected": true,
    "co2": "1,40"
  },
  {
    "fruit_id": 234528,
    "name": "clementina calabrese",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-clementina_tree.png",
    "number": 20,
    "co2_unit": 0.06,
    "farmer_name": "Az. Agricola Paolo Maria Lamenza",
    "location_country": "Corigliano-Rossano, Calabria",
    "location_province": "Corigliano-Rossano, Cosenza (CS)",
    "selected": true,
    "co2": "1,20"
  },
  {
    "fruit_id": 74882,
    "name": "mandarino tardivo di ciaculli",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595409710/fruits/biorfarm-icon-mandarino-tardivo-di-ciaculli_tree.png",
    "number": 20,
    "co2_unit": 0.06077,
    "farmer_name": "Fratelli Marceno",
    "location_country": " Palermo, Sicilia",
    "location_province": "Palermo (PA)",
    "selected": true,
    "co2": "1,22"
  },
  {
    "fruit_id": 63726,
    "name": "mela stark",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1586198673/fruits/biorfarm-icon-mela-stark_tree.png",
    "number": 20,
    "co2_unit": 0.04,
    "farmer_name": "Paolo Rossi",
    "location_country": "Clès, Trentino",
    "location_province": "Val di Non, Trento (TN)",
    "selected": true,
    "co2": "0,80"
  }
]
