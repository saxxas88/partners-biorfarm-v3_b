/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 150358,
    "name": "olivo evo DOP tuscia",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1634203069/fruits/biorfarm-icon-olivo-evo-dop-tuscia_tree.png",
    "number": 50,
    "co2_unit": 0.3,
    "farmer_name": "Giorgio Grani",
    "location_country": "Viterbo, Lazio",
    "location_province": "Viterbo, (VT)",
    "selected": true,
    "co2": "15,00"
  },
  {
    "fruit_id": 226603,
    "name": "Kiwi Hayward",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1581586234/fruits/biorfarm-icon-kiwi_tree.png",
    "number": 100,
    "co2_unit": 0.00115,
    "farmer_name": "Demetrio Parlapiano",
    "location_country": "Cisternina di Latina, Lazio",
    "location_province": "Cisternina di Latina, Latina (LT)",
    "selected": true,
    "co2": "0,11"
  },
  {
    "fruit_id": 119855,
    "name": "alveare castagno",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1607338355/fruits/biorfarm-icon-alveare-castagno_tree.png",
    "number": 50,
    "co2_unit": 0,
    "farmer_name": "Marcoraniero Melani",
    "location_country": "Collegiove, Lazio",
    "location_province": "Collegiove, Rieti (RI)",
    "selected": true,
    "co2": "0,00"
  },
  {
    "fruit_id": 152063,
    "name": "alveare millefiori",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1607338355/fruits/biorfarm-icon-alveare-millefiori_tree.png",
    "number": 50,
    "co2_unit": 0,
    "farmer_name": "Leonardo Attiani",
    "location_country": "Valmontone, Lazio",
    "location_province": "Valmontone, Roma (RM)",
    "selected": true,
    "co2": "0,00"
  }
]