/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = 
[
  {
    "fruit_id": 5047,
    "name": "albicocca pellecchiella",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477692/fruits/biorfarm-icon-albicocca-pellecchiella_tree.png",
    "number": 5,
    "co2_unit": 0.07,
    "farmer_name": "Famiglia Dell'Orto",
    "location_country": "Piana del Sele, Campania",
    "location_province": "Piana del Sele, Salerno (SA)",
    "selected": true,
    "co2": "0,35"
  },
  {
    "fruit_id": 116257,
    "name": "albicocca rossa",
    "icon": "https://res.cloudinary.com/biorfarm/image/upload/v1561625041/fruits/biorfarm-icon-albicocca_rossa_fruit.png",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-albicocca-rossa_tree.png",
    "img_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1562324133/fruits/albicocca_rossa_biorfarm_frutta.jpg",
    "number": 5,
    "co2_unit": 0.07,
    "farmer_name": "Famiglia Dell'Orto",
    "location_country": "Piana del Sele, Campania",
    "location_province": "Piana del Sele, Salerno (SA)",
    "selected": true,
    "co2": "0,35"
  },
  {
    "fruit_id": 102377,
    "name": "arancia tarocco gallo",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1608220860/fruits/biorfarm-icon-arancia-tarocco-gallo_tree.png",
    "number": 5,
    "co2_unit": 0.07,
    "farmer_name": "Emanuele Calvo",
    "location_country": "Mineo, Sicilia",
    "location_province": "Mineo, Catania (CT)",
    "selected": true,
    "co2": "0,35"
  },
  {
    "fruit_id": 683,
    "name": "clementina calabrese",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-clementina_tree.png",
    "number": 5,
    "co2_unit": 0.06,
    "farmer_name": "Paolo De Falco",
    "location_country": "Corigliano-Rossano, Calabria",
    "location_province": "Piana di Sibari, Corigliano-Rossano (CS)",
    "selected": true,
    "co2": "0,30"
  },
  {
    "fruit_id": 113208,
    "name": "clementina tardiva",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1610451045/fruits/biorfarm-icon-clementina-tardiva_tree.png",
    "number": 5,
    "co2_unit": 0.065,
    "farmer_name": "Az. Agricola Paolo Maria Lamenza",
    "location_country": "Corigliano-Rossano, Calabria",
    "location_province": "Corigliano-Rossano, Cosenza (CS)",
    "selected": true,
    "co2": "0,33"
  },
  {
    "fruit_id": 114606,
    "name": "mela fujion",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1610988794/fruits/biorfarm-icon-mela_fujion_tree.png",
    "number": 5,
    "co2_unit": 0.04,
    "farmer_name": "Az. Agricola Famiglia Casarotti",
    "location_country": "Caldiero, Veneto",
    "location_province": "Caldiero, Verona (VR)",
    "selected": true,
    "co2": "0,20"
  },
  {
    "fruit_id": 127213,
    "name": "pera williams",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-pera_williams_tree.png",
    "number": 5,
    "co2_unit": 0.046,
    "farmer_name": "Mirko Bezzi",
    "location_country": "Urbana, Veneto",
    "location_province": "Urbana, Padova (PD)",
    "selected": true,
    "co2": "0,23"
  },
  {
    "fruit_id": 11701,
    "name": "pesca gialla",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-pesca_gialla_tree.png",
    "number": 5,
    "co2_unit": 0.063,
    "farmer_name": "Fratelli Brero",
    "location_country": "Lagnasco, Piemonte",
    "location_province": "Lagnasco, Cuneo (CN)",
    "selected": true,
    "co2": "0,32"
  },
  {
    "fruit_id": 17811,
    "name": "susina nera",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-susina_nera_tree.png",
    "number": 5,
    "co2_unit": 0.024,
    "farmer_name": "Daniele Bucci",
    "location_country": "Faenza, Emilia Romagna",
    "location_province": "Faenza, Ravenna (RA)",
    "selected": true,
    "co2": "0,12"
  },
  {
    "fruit_id": 32926,
    "name": "uva vittoria",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1591870312/fruits/biorfarm-icon-uva-bianca_tree.png",
    "number": 5,
    "co2_unit": 0.04,
    "farmer_name": "Antonia Giannella",
    "location_country": "San Ferdinando, Puglia",
    "location_province": "San Ferdinando di Puglia, Barletta-Andria-Trani (BT)",
    "selected": true,
    "co2": "0,20"
  }
]