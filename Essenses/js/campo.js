/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees= [
  {
    "fruit_id": 683,
    "name": "clementina calabrese",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-clementina_tree.png",
    "number": 60,
    "co2_unit": 0.06,
    "farmer_name": "Paolo De Falco",
    "location_country": "Corigliano-Rossano, Calabria",
    "location_province": "Piana di Sibari, Corigliano-Rossano (CS)",
    "selected": true,
    "co2": "3,6"
  },
  {
    "fruit_id": 112368,
    "name": "cachi di romagna",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1583752500/fruits/biorfarm-icon-caco-loto-di-romagna_tree.png",
    "number": 20,
    "co2_unit": 0.0206,
    "farmer_name": "Famiglia Scardovi",
    "location_country": "Faenza, Emilia Romagna",
    "location_province": "Faenza, Ravenna (RA)",
    "selected": true,
    "co2": "0,4"
  },
  {
    "fruit_id": 89695,
    "name": "alveare castagno",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1606987616/fruits/biorfarm-icon-alveare-castagno_tree.png",
    "number": 20,
    "co2_unit": 0,
    "farmer_name": "Famiglia Nucci",
    "location_country": "Santarcangelo di Romagna, Emilia-Romagna",
    "location_province": "Santarcangelo di Romagna, Rimini (RN)",
    "selected": true,
    "co2": "0,0"
  }
  
]