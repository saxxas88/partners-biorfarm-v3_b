/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees=[

  {
   "fruit_id": 682,
   "number": 70,
   "co2": "4,90",
   "location_country": "Corigliano-Rossano, Calabria",
   "location_province": "Piana di Sibari, Corigliano-Rossano (CS)"
 } , 
  {
   "fruit_id": 683,
   "number": 70,
   "co2": "4,20",
   "location_country": "Corigliano-Rossano, Calabria",
   "location_province": "Piana di Sibari, Corigliano-Rossano (CS)"
 } , 
  {
   "fruit_id": 38775,
   "number": 70,
   "co2": "4,76",
   "location_country": "Ardore, Calabria",
   "location_province": "Ardore, Reggio Calabria (RC)"
 } , 
  {
   "fruit_id": 60809,
   "number": 70,
   "co2": "2,80",
   "location_country": "Barge, Piemonte",
   "location_province": "Barge, Cuneo (CN)"
 } , 
  {
   "fruit_id": 62374,
   "number": 70,
   "co2": "0,08",
   "location_country": "Teverola, Campania",
   "location_province": "Teverola, Caserta (CE)"
 } , 
  {
   "fruit_id": 72045,
   "number": 75,
   "co2": "1,35",
   "location_country": "Murozzano, Piemonte",
   "location_province": "Murazzano, Cuneo (CN)"
 } , 
  {
   "fruit_id": 78290,
   "number": 75,
   "co2": "10,16",
   "location_country": "Altomonte, Calabria",
   "location_province": "Altomonte, Cosenza (CS)"
 } , 
  {
   "fruit_id": 119864,
   "number": 50,
   "co2": "0,05",
   "location_country": "Collegiove, Lazio",
   "location_province": "Collegiove, Rieti (RI)"
 } , 
  {
   "fruit_id": 119648,
   "number": 50,
   "co2": "0,05",
   "location_country": "Genova, Liguria",
   "location_province": "Genova (GE)"
 }  
 
 ]