/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [

  {
    "fruit_id": 11636,
    "number": 25,
    "co2": "1,3", //1,25
    "location_country": "Lagnasco, Piemonte",
    "location_province": "Lagnasco, Cuneo (CN)"
  },
  {
    "fruit_id": 11701,
    "number": 25,
    "co2": "2,3", //2,25
    "location_country": "Lagnasco, Piemonte",
    "location_province": "Lagnasco, Cuneo (CN)"
  },
  {
    "fruit_id": 17811,
    "number": 25,
    "co2": "2,0",//1,75
    "location_country": "Faenza, Emilia Romagna",
    "location_province": "Faenza (RA)"
  },
  {
    "fruit_id": 17812,
    "number": 25,
    "co2": "2,3",//2,25
    "location_country": "Faenza, Emilia Romagna",
    "location_province": "Faenza (RA)"
  }
]