/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 75681,
    "name": "lampone rosso",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1596010042/fruits/biorfarm-icon-lampone-rosso_tree.png",
    "number": 30,
    "co2_unit": 0.00167,
    "farmer_name": "Leonardo Dorigo",
    "location_country": "Monteleone D'Orvieto, Umbria",
    "location_province": "Monteleone D'Orvieto, Terni (TR)",
    "selected": true,
    "co2": "0,05"
  },
  {
    "fruit_id": 139287,
    "name": "melograno dente di cavallo",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1622824926/fruits/biorfarm-icon-melograno-dente-di-cavallo_tree.png",
    "number": 30,
    "co2_unit": 0.05,
    "farmer_name": "Famiglia Bonora",
    "location_country": "Castellaneta, Puglia",
    "location_province": "Castellaneta, Taranto (TA)",
    "selected": true,
    "co2": "1,50"
  },
  {
    "fruit_id": 119855,
    "name": "alveare castagno",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1607338355/fruits/biorfarm-icon-alveare-castagno_tree.png",
    "number": 15,
    "co2_unit": 0,
    "farmer_name": "Marcoraniero Melani",
    "location_country": "Collegiove, Lazio",
    "location_province": "Collegiove, Rieti (RI)",
    "selected": true,
    "co2": "0,00"
  },
  {
    "fruit_id": 152063,
    "name": "alveare millefiori",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1607338355/fruits/biorfarm-icon-alveare-millefiori_tree.png",
    "number": 15,
    "co2_unit": 0,
    "farmer_name": "Leonardo Attiani",
    "location_country": "Valmontone, Lazio",
    "location_province": "Valmontone, Roma (RM)",
    "selected": true,
    "co2": "0,00"
  }
]