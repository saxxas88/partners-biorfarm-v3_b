/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 37211,
    "name": "castagna palummina",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-castagna_tree.png",
    "number": 30,
    "co2_unit": 1.589,
    "farmer_name": "Alfredo Di Cicilia",
    "location_country": "Montella, Campania",
    "location_province": "Montella, Avellino (AV)",
    "selected": true,
    "co2": "47,67"
  },
  {
    "fruit_id": 156693,
    "name": "mandorla a cuore dei monti nebrodi",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mandorla_tree.png",
    "number": 20,
    "co2_unit": 0.17003,
    "farmer_name": "Famiglia Virzì",
    "location_country": "Cesarò, Sicilia",
    "location_province": "Cesarò, Messina (ME)",
    "selected": true,
    "co2": "3,40"
  },
  {
    "fruit_id": 238518,
    "name": "noce in guscio essiccata",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1622821393/fruits/biorfarm-icon-noce-essiccata_tree.png",
    "number": 30,
    "co2_unit": 0.83943,
    "farmer_name": "Elsa e Denis Fabbri",
    "location_country": "Forlì, Emilia-Romagna",
    "location_province": "Forlì (FC)",
    "selected": true,
    "co2": "25,18"
  },
  {
    "fruit_id": 28708,
    "name": "olivo coratina",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1594972020/fruits/biorfarm-icon-olivo-coratina_tree.png",
    "number": 20,
    "co2_unit": 0.3,
    "farmer_name": "Famiglia Lobascio",
    "location_country": "Minervino, Puglia",
    "location_province": "Minervino, Barletta-Andria-Trani (BT)",
    "selected": true,
    "co2": "6,00"
  }
]