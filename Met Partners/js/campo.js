/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 127329,
    "name": "mela golden delicious",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mela_golden_tree.png",
    "number": 25,
    "co2_unit": 0.04,
    "farmer_name": "Mirko Bezzi",
    "location_country": "Urbana, Veneto",
    "location_province": "Urbana, Padova (PD)",
    "selected": true,
    "co2": "1,00"
  }
]
