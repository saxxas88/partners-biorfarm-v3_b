const partner_trees = [
  {
    "fruit_id": 167651,
    "name": "arancia biondo della riviera dei gelsomini",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1639568082/fruits/biorfarm-icon-arancia-biondo_tree.png",
    "number": 10,
    "co2_unit": 0.07,
    "farmer_name": "Giovanni Zappavigna",
    "location_country": "Ardore, Calabria",
    "location_province": "Ardore, Reggio Calabria (RC)",
    "selected": true,
    "co2": "0,70"
  },
  {
    "fruit_id": 227725,
    "name": "arancia fukumoto",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1639568082/fruits/biorfarm-icon-arancia-biondo_tree.png",
    "number": 10,
    "co2_unit": 0.07,
    "farmer_name": "Famiglia D'Onghia",
    "location_country": "Palagianello, Puglia",
    "location_province": "Palagianello, Taranto (TA)",
    "selected": true,
    "co2": "0,70"
  },
  {
    "fruit_id": 682,
    "name": "arancia navelina",
    "icon": "https://res.cloudinary.com/biorfarm/image/upload/v1561625041/fruits/biorfarm-icon-arancia_navelina_fruit.png",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477692/fruits/biorfarm-icon-arancia_navelina_tree.png",
    "img_tree": "https://www.biorfarm.com/wp-content/uploads/2020/03/Arance-Navel-di-Sicilia.jpg",
    "number": 10,
    "co2_unit": 0.07,
    "farmer_name": "Paolo De Falco",
    "location_country": "Corigliano-Rossano, Calabria",
    "location_province": "Piana di Sibari, Corigliano-Rossano (CS)",
    "selected": true,
    "co2": "0,70"
  },
  {
    "fruit_id": 14648,
    "name": "arancia valencia",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477692/fruits/biorfarm-icon-arancia_valencia_tree.png",
    "number": 10,
    "co2_unit": 0.07,
    "farmer_name": "Famiglia Leone",
    "location_country": "Noto, Sicilia",
    "location_province": "Noto, Siracusa (SR)",
    "selected": true,
    "co2": "0,70"
  },
  {
    "fruit_id": 224750,
    "name": "giuggiola",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1593708292/fruits/biorfarm-icon-giuggiola_tree.png",
    "number": 10,
    "co2_unit": 0.09597,
    "farmer_name": "Azienda Agricola Buccolini",
    "location_country": "Macerata, Marche",
    "location_province": "Macerata (MC)",
    "selected": true,
    "co2": "0,96"
  },
  {
    "fruit_id": 61443,
    "name": "kiwi boerica",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1581586234/fruits/biorfarm-icon-kiwi_tree.png",
    "number": 10,
    "co2_unit": 0.00115,
    "farmer_name": "Azienda Agricola Cordopatri",
    "location_country": "Taurianova, Calabria",
    "location_province": "Taurianova, Reggio Calabria (RC)",
    "selected": true,
    "co2": "0,01"
  },
  {
    "fruit_id": 38775,
    "name": "limone di calabria",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-limone-zagara-bianca_tree.png",
    "number": 10,
    "co2_unit": 0.068,
    "farmer_name": "Giovanni Zappavigna",
    "location_country": "Ardore, Calabria",
    "location_province": "Ardore, Reggio Calabria (RC)",
    "selected": true,
    "co2": "0,68"
  },
  {
    "fruit_id": 156501,
    "name": "mela morgenduft dallago",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1637681977/fruits/biorfarm-icon-mela-morgenduft-dallago_tree.png",
    "number": 10,
    "co2_unit": 0.04,
    "farmer_name": "Diego Boggian",
    "location_country": "Legnago, Veneto",
    "location_province": "Legnago, Verona (VR)",
    "selected": true,
    "co2": "0,40"
  },
  {
    "fruit_id": 63726,
    "name": "mela stark",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1586198673/fruits/biorfarm-icon-mela-stark_tree.png",
    "number": 10,
    "co2_unit": 0.04,
    "farmer_name": "Paolo Rossi",
    "location_country": "Clès, Trentino",
    "location_province": "Val di Non, Trento (TN)",
    "selected": true,
    "co2": "0,40"
  },
  {
    "fruit_id": 71557,
    "name": "pera williams",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-pera_williams_tree.png",
    "number": 10,
    "co2_unit": 0.046,
    "farmer_name": "Lorenzo Sacchetto",
    "location_country": "Lagnasco, Piemonte",
    "location_province": "Lagnasco, Cuneo (CN)",
    "selected": true,
    "co2": "0,46"
  },
  {
    "fruit_id": 225112,
    "name": "susina stanley",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-susina_nera_tree.png",
    "number": 10,
    "co2_unit": 0.024,
    "farmer_name": "Famiglia Montecchia",
    "location_country": "Morro D'Oro, Abruzzo",
    "location_province": "Morro D'Oro,Teramo (TE)",
    "selected": true,
    "co2": "0,24"
  },
  {
    "fruit_id": 78574,
    "name": "uva bianca moscato",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1599730568/fruits/biorfarm-icon-uva-bianca-moscato_tree.png",
    "number": 10,
    "co2_unit": 0.04,
    "farmer_name": "Fabio Proverbio",
    "location_country": "Treviglio, Lombardia",
    "location_province": "Treviglio, Bergamo (BG)",
    "selected": true,
    "co2": "0,40"
  }
]