/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 154604,
    "name": "olivo dei monti nebrodi",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-olivo_tree.png",
    "number": 21,
    "co2_unit": 0.3,
    "farmer_name": "Famiglia Virzì",
    "location_country": "Cesarò, Sicilia",
    "location_province": "Cesarò, Messina (ME)",
    "selected": true,
    "co2": "6,30"
  }
]
