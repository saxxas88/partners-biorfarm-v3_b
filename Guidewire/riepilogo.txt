CO2 totali: 6.300 Kg

Agricoltori supportati: 1

Alberi in partnerships: 21

Codice Sconto: no

Page URL: https://www.biorfarm.com/guidewire-italia/

Logo URL: https://res.cloudinary.com/biorfarm/image/upload/v1656601771/static/guidewire_software_logo.svg

Banner URL: 

Slogan: "Mai come quest'anno e i prossimi a venire, il tema della salvaguardia ambientale è al centro dell'attenzione mondiale.
Esattamente come il contributo di ogni dipendente è critico per il successo di Guidewire, così anche l'impegno di ogni singolo individuo nel quotiano può creare un impatto positivo nella preservazione dell'ambiente.
Guidewire e l'Italian Social Committee approfittano della chiusura dell'anno fiscale per augurarti una buona estate e per omaggiarti con un piccolo gesto a favore del nostro pianeta."