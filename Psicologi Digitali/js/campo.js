/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 162058,
    "name": "arancia rossa di sicilia IGP",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1638871720/fruits/biorfarm-icon-arancia-rossa_tree.png",
    "number": 2,
    "co2_unit": 0.07,
    "farmer_name": "Azienda Agricola Parisi",
    "location_country": "Lentini, Sicilia",
    "location_province": "Lentini, Siracusa (SR)",
    "selected": true,
    "co2": "0,140"
  },
  {
    "fruit_id": 82556,
    "name": "baby kiwi",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1602753297/fruits/biorfarm-icon-baby_kiwi_tree.png",
    "number": 5,
    "co2_unit": 0.00115,
    "farmer_name": "Fabio Proverbio",
    "location_country": "Treviglio, Lombardia",
    "location_province": "Treviglio, Bergamo (BG)",
    "selected": true,
    "co2": "0,006"
  },
  {
    "fruit_id": 112368,
    "name": "cachi di romagna",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1583752500/fruits/biorfarm-icon-caco-loto-di-romagna_tree.png",
    "number": 1,
    "co2_unit": 0.0206,
    "farmer_name": "Famiglia Scardovi",
    "location_country": "Faenza, Emilia Romagna",
    "location_province": "Faenza, Ravenna (RA)",
    "selected": true,
    "co2": "0,021"
  },
  {
    "fruit_id": 60703,
    "name": "germogli di bambù",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1582824359/fruits/biorfarm-icon-germogli-di-bambu_tree.png",
    "number": 2,
    "co2_unit": 0.002,
    "farmer_name": "Famiglia Ferri",
    "location_country": "Castel Rozzone, Lombardia",
    "location_province": "Castel Rozzone, Bergamo (BG)",
    "selected": true,
    "co2": "0,004"
  },
  {
    "fruit_id": 11636,
    "name": "pera williams",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-pera_williams_tree.png",
    "number": 3,
    "co2_unit": 0.046,
    "farmer_name": "Fratelli Brero",
    "location_country": "Lagnasco, Piemonte",
    "location_province": "Lagnasco, Cuneo (CN)",
    "selected": true,
    "co2": "0,138"
  },
  {
    "fruit_id": 78574,
    "name": "uva bianca moscato",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1599730568/fruits/biorfarm-icon-uva-bianca-moscato_tree.png",
    "number": 13,
    "co2_unit": 0.04,
    "farmer_name": "Fabio Proverbio",
    "location_country": "Treviglio, Lombardia",
    "location_province": "Treviglio, Bergamo (BG)",
    "selected": true,
    "co2": "0,520"
  },
  {
    "fruit_id": 76304,
    "name": "pesca gialla toscana",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-pesca_gialla_tree.png",
    "number": 2,
    "co2_unit": 0.063,
    "farmer_name": "Famiglia Checcucci",
    "location_country": "Roccastrada, Toscana",
    "location_province": "Roccastrada, Grosseto (GR)",
    "selected": true,
    "co2": "0,126"
  }
]