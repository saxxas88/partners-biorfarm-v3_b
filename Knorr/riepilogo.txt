CO2 totali: 5.020 Kg

Agricoltori supportati: 4

Alberi in partnerships: 85

Codice Sconto: no

FORM: no

Page URL: https://www.biorfarm.com/knorr/

Logo URL: https://res.cloudinary.com/biorfarm/image/upload/v1694510389/static/logo-knorr.png

Banner URL: 

Slogan: "Una scelta legata al buon cibo
Un’alimentazione sostenibile e ricca di gusto è alla base di un futuro migliore. Partendo dalle scelte che facciamo ogni giorno a tavola possiamo cambiare il mondo in cui viviamo.
Chi si unisce a questo percorso sceglie infatti ingredienti nutrienti, economici, gustosi e soprattutto con un minore impatto ambientale. Ma soprattutto sceglie di dare il proprio contributo al cambiamento attraverso scelte quotidiane che seppur apparentemente semplici, hanno un grande impatto sia su di noi che sul mondo che ci circonda.
Conosci da vicino il nostro progetto e scarica il ricettario del Buon Cibo su knorr.com/it/buoncibo.html"