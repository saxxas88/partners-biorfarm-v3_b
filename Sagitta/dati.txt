/sagitta

Tree – x qty

2	Arancia Tarocco (Arcoria)
2	Campo di Grani antichi Gentil Tramazzo – Farina Integrale (Mazzi)
2	Clementina Tardiva (Messina)
1	Fico d'India Rosso (Bruno)
3	Limone Zagara Bianca (Lo Sciotto)
3	Mandarino Calabrese (Zappavigna)
2	Mandarino Tardivo di Ciaculli (Marcenò)
2	Mela Gala (Casarotti)
2	Mela Stark (Rossi)
2	Olivo Coratina (Lobascio)
1	Olivo EVO Dop Tuscia (Grani)
4	Olivo IGP Toscano (Checcucci)
2	Pera Abate (Sacchetto)
1	Pera williams (Sacchetto)
2	Pompelmo Rosso Star Ruby (Calvo)
1	Susina Stanley (Montecchia)
1	Alveare millefiori (De Pascale)
2	Mandorla Sgusciata dell’Alta Murgia (Terradiva)
2	Castagna Palummina (Di Cicilia)
1	Arancia Fukumoto (D’Onghia)
1	Uva Bianca Moscato (Proverbio)
1	Melograno wonderful (Costantino)
1	Mela Golden Delicious (Rossi)
1	Albicocca Rossa Precoce (Dell’Orto)
1	Cachi di Romagna (Golinelli)
1	Kiwi Boerica (Cordopatri)
1	Nettarina di Faenza (Bucci)