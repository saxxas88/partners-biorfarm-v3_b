/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 162058,
    "name": "arancia rossa di sicilia IGP",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1638871720/fruits/biorfarm-icon-arancia-rossa_tree.png",
    "number": 263,
    "co2_unit": 0.07,
    "farmer_name": "Azienda Agricola Parisi",
    "location_country": "Lentini, Sicilia",
    "location_province": "Lentini, Siracusa (SR)",
    "selected": true,
    "co2": "18,41"
  }
]