CO2 totali: 18.410 Kg

Agricoltori supportati: 1

Alberi in partnerships: 263

Codice Sconto: NO

Form: YES

Page URL: https://www.biorfarm.com/liparipeople/

Logo URL: https://res.cloudinary.com/biorfarm/image/upload/v1734080738/static/lipari-logo.png

Banner URL: 

Slogan: Nell’Aranceto Lipari, è stato adottato un albero per ogni talento in azienda!
Con l’occasione del Natale, ti omaggiamo di una pianta di arance per ringraziarti per questo anno trascorso insieme. I suoi frutti, una volta raccolti, ti saranno consegnati per condividerli con i tuoi cari.