CO2 totali: 420 Kg

Agricoltori supportati: 1

Alberi in partnerships: 6

Codice Sconto: no

Page URL: https://www.biorfarm.com/santacecilia/

Logo URL: https://res.cloudinary.com/biorfarm/image/upload/v1668701512/static/santa_cecilia-logo.svg

Banner URL: no

Slogan: "Relazione e cura sono da sempre le parole chiave del lavoro di Punto Service, con le persone così come verso l’ambiente. Per questo abbiamo scelto di dare un importante contributo attraverso la costituzione di questo frutteto aziendale. L’obiettivo è sostenere i piccoli agricoltori, ridurre l’impatto ambientale e regalare un’esperienza unica agli adottanti, ospiti e soci. Oltre a ridurre l’impatto delle nostre attività, vogliamo impegnarci a pesare meno sul pianeta che è la casa di tutti noi. Curare un albero è tramandare la memoria, promuovere la crescita e fare in modo che i frutti di oggi siano a beneficio del mondo che verrà.

“Quando le radici sono profonde non c’è motivo di temere il vento”
(proverbio africano)"