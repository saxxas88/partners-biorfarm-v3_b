/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 162058,
    "name": "arancia rossa di sicilia IGP",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1638871720/fruits/biorfarm-icon-arancia-rossa_tree.png",
    "number": 8,
    "co2_unit": 0.07,
    "farmer_name": "Azienda Agricola Parisi",
    "location_country": "Lentini, Sicilia",
    "location_province": "Lentini, Siracusa (SR)",
    "selected": true,
    "co2": "0,560"
  },
  {
    "fruit_id": 72306,
    "name": "feijoa",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1593708973/fruits/biorfarm-icon-feijoa_tree.png",
    "number": 1,
    "co2_unit": 0.10535,
    "farmer_name": "Emanuela Cavallaro",
    "location_country": "Sant'Onofrio, Calabria",
    "location_province": "Sant'Onofrio, Vibo Valentia (VV)",
    "selected": true,
    "co2": "0,105"
  },
  {
    "fruit_id": 226551,
    "name": "guava",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1637321015/fruits/biorfarm-icon-guava_tree.png",
    "number": 1,
    "co2_unit": 0.03,
    "farmer_name": "Famiglia Vitrano",
    "location_country": "Castelvetrano, Sicilia",
    "location_province": "Castelvetrano, Trapani (TP)",
    "selected": true,
    "co2": "0,030"
  },
  {
    "fruit_id": 234309,
    "name": "limone costa d'amalfi igp",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1639130191/fruits/biorfarm-icon-limone_tree.png",
    "number": 1,
    "co2_unit": 0.068,
    "farmer_name": "Salvatore Aceto",
    "location_country": "Amalfi, Campania",
    "location_province": "Amalfi, Salerno (SA)",
    "selected": true,
    "co2": "0,068"
  },
  {
    "fruit_id": 681,
    "name": "mela fuji",
    "icon": "https://res.cloudinary.com/biorfarm/image/upload/v1561625042/fruits/biorfarm-icon-mela_fuji_fruit.png",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mela_fuji_tree.png",
    "img_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1500289952/fruits/mela_fuji_biorfarm_frutta.jpg",
    "number": 2,
    "co2_unit": 0.04,
    "farmer_name": "Paolo Rossi",
    "location_country": "Clès, Trentino",
    "location_province": "Val di Non, Clès (TN)",
    "selected": true,
    "co2": "0,080"
  },
  {
    "fruit_id": 139232,
    "name": "noce essiccata",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1622821393/fruits/biorfarm-icon-noce-essiccata_tree.png",
    "number": 1,
    "co2_unit": 0.83943,
    "farmer_name": "Claudio Granelli",
    "location_country": "Varano de Melegari, Emilia-Romagna",
    "location_province": "Varano de Melegari, Parma (PR)",
    "selected": true,
    "co2": "0,839"
  },
  {
    "fruit_id": 232810,
    "name": "peperoncino calabrese piccante fresco",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1665676060/fruits/biorfarm-icon-peperoncino_tree.png",
    "number": 1,
    "co2_unit": 0.00084,
    "farmer_name": "Ilario Lamonaca",
    "location_country": "Caulonia, Calabria",
    "location_province": "Caulonia, Reggio Calabria (RC)",
    "selected": true,
    "co2": "0,001"
  },
  {
    "fruit_id": 63670,
    "name": "susina sorriso di primavera",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1586198672/fruits/biorfarm-icon-susina-sorriso-di-primavera_tree.png",
    "number": 3,
    "co2_unit": 0.024,
    "farmer_name": "Famiglia Dell'Orto",
    "location_country": "Piana del Sele, Campania",
    "location_province": "Piana del Sele, Salerno (SA)",
    "selected": true,
    "co2": "0,072"
  }
]