/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 75225,
    "name": "arancia tarocco sant'alfio",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595581977/fruits/biorfarm-icon-arancia-tarocco-sant-alfio_tree.png",
    "number": 25,
    "co2_unit": 0.07,
    "farmer_name": "Emanuele Calvo",
    "location_country": "Mineo, Sicilia",
    "location_province": "Mineo, Catania (CT)",
    "selected": true,
    "co2": "1,75"
  },
  {
    "fruit_id": 164404,
    "name": "limone femminello",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1639130191/fruits/biorfarm-icon-limone_tree.png",
    "number": 10,
    "co2_unit": 0.068,
    "farmer_name": "Paolo De Falco",
    "location_country": "Corigliano-Rossano, Calabria",
    "location_province": "Piana di Sibari, Corigliano-Rossano (CS)",
    "selected": true,
    "co2": "0,68"
  },
  {
    "fruit_id": 33671,
    "name": "limone zagara bianca",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-limone-zagara-bianca_tree.png",
    "number": 10,
    "co2_unit": 0.068,
    "farmer_name": "Fratelli Cupane",
    "location_country": "Rocca di Capri Leone, Sicilia",
    "location_province": "Rocca di Capri Leone, Messina (ME)",
    "selected": true,
    "co2": "0,68"
  },
  {
    "fruit_id": 33356,
    "name": "melograno wonderful",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-melograno-wonderful_tree.png",
    "number": 25,
    "co2_unit": 0.05,
    "farmer_name": "Giuseppe Costantino",
    "location_country": "Alcamo, Sicilia",
    "location_province": "Alcamo, Trapani (TP)",
    "selected": true,
    "co2": "1,25"
  }
]