/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 167651,
    "name": "arancia biondo della riviera dei gelsomini",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1639568082/fruits/biorfarm-icon-arancia-biondo_tree.png",
    "number": 105,
    "co2_unit": 0.07,
    "farmer_name": "Giovanni Zappavigna",
    "location_country": "Ardore, Calabria",
    "location_province": "Ardore, Reggio Calabria (RC)",
    "selected": true,
    "co2": "7,35"
  },
  {
    "fruit_id": 232769,
    "name": "arancia tarocco",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-arancia-tarocco_tree.png",
    "number": 105,
    "co2_unit": 0.07,
    "farmer_name": "Ilario Lamonaca",
    "location_country": "Caulonia, Calabria",
    "location_province": "Caulonia, Reggio Calabria (RC)",
    "selected": true,
    "co2": "7,35"
  },
  {
    "fruit_id": 113208,
    "name": "clementina tardiva",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1610451045/fruits/biorfarm-icon-clementina-tardiva_tree.png",
    "number": 105,
    "co2_unit": 0.065,
    "farmer_name": "Az. Agricola Paolo Maria Lamenza",
    "location_country": "Corigliano-Rossano, Calabria",
    "location_province": "Corigliano-Rossano, Cosenza (CS)",
    "selected": true,
    "co2": "6,83"
  },
  {
    "fruit_id": 74882,
    "name": "mandarino tardivo di ciaculli",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595409710/fruits/biorfarm-icon-mandarino-tardivo-di-ciaculli_tree.png",
    "number": 105,
    "co2_unit": 0.06077,
    "farmer_name": "Fratelli Marceno",
    "location_country": " Palermo, Sicilia",
    "location_province": "Palermo (PA)",
    "selected": true,
    "co2": "6,38"
  }
]