/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = 
[
  {
    "fruit_id": 14648,
    "name": "arancia valencia",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477692/fruits/biorfarm-icon-arancia_valencia_tree.png",
    "number": 75,
    "co2_unit": 0.07,
    "farmer_name": "Famiglia Leone",
    "location_country": "Noto, Sicilia",
    "location_province": "Noto, Siracusa (SR)",
    "selected": true,
    "co2": "5,25"
  },
  {
    "fruit_id": 102169,
    "name": "arancia valencia calvo",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477692/fruits/biorfarm-icon-arancia_valencia_tree.png",
    "number": 75,
    "co2_unit": 0.07,
    "farmer_name": "Emanuele Calvo",
    "location_country": "Mineo, Sicilia",
    "location_province": "Mineo, Catania (CT)",
    "selected": true,
    "co2": "5,25"
  },
  {
    "fruit_id": 120210,
    "name": "avocado bacon",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1614620101/fruits/biorfarm-icon-avocado-bacon_tree.png",
    "number": 50,
    "co2_unit": 0.036,
    "farmer_name": "Famiglia Campisi",
    "location_country": "Cassibile, Sicilia",
    "location_province": "Cassibile, Siracusa (SR)",
    "selected": true,
    "co2": "1,80"
  },
  {
    "fruit_id": 33667,
    "name": "mango tommy atkins",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-mango-tommy_tree.png",
    "number": 50,
    "co2_unit": 0.01015,
    "farmer_name": "Fratelli Cupane",
    "location_country": "Rocca di Capri Leone, Sicilia",
    "location_province": "Rocca di Capri Leone, Messina (ME)",
    "selected": true,
    "co2": "0,51"
  }
]