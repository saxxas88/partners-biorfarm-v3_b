/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 167651,
    "name": "arancia biondo della riviera dei gelsomini",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1639568082/fruits/biorfarm-icon-arancia-biondo_tree.png",
    "number": 1,
    "co2_unit": 0.07,
    "farmer_name": "Giovanni Zappavigna",
    "location_country": "Ardore, Calabria",
    "location_province": "Ardore, Reggio Calabria (RC)",
    "selected": true,
    "co2": "0,070"
  },
  {
    "fruit_id": 129165,
    "name": "arancia tarocco",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-arancia-tarocco_tree.png",
    "number": 1,
    "co2_unit": 0.07,
    "farmer_name": "Angela Arcoria",
    "location_country": "Paternò, Sicilia",
    "location_province": "Paternò, Catania (CT)",
    "selected": true,
    "co2": "0,070"
  },
  {
    "fruit_id": 121580,
    "name": "clementina tardiva",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1610451045/fruits/biorfarm-icon-clementina-tardiva_tree.png",
    "number": 1,
    "co2_unit": 0.06,
    "farmer_name": "Bruno Messina",
    "location_country": "Rosarno, Calabria",
    "location_province": "Rosarno, Reggio Calabria (RC)",
    "selected": true,
    "co2": "0,060"
  },
  {
    "fruit_id": 61443,
    "name": "kiwi boerica",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1581586234/fruits/biorfarm-icon-kiwi_tree.png",
    "number": 1,
    "co2_unit": 0.00115,
    "farmer_name": "Azienda Agricola Cordopatri",
    "location_country": "Taurianova, Calabria",
    "location_province": "Taurianova, Reggio Calabria (RC)",
    "selected": true,
    "co2": "0,001"
  },
  {
    "fruit_id": 62374,
    "name": "kiwi hayward",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1581586234/fruits/biorfarm-icon-kiwi_tree.png",
    "number": 1,
    "co2_unit": 0.00115,
    "farmer_name": "Azienda Agricola Masseria Cesaro",
    "location_country": "Teverola, Campania",
    "location_province": "Teverola, Caserta (CE)",
    "selected": true,
    "co2": "0,001"
  },
  {
    "fruit_id": 120232,
    "name": "limone verdello di siracusa igp",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1572517416/fruits/biorfarm-icon-limone_verdello_tree.png",
    "number": 1,
    "co2_unit": 0.068,
    "farmer_name": "Famiglia Campisi",
    "location_country": "Cassibile, Sicilia",
    "location_province": "Cassibile, Siracusa (SR)",
    "selected": true,
    "co2": "0,068"
  },
  {
    "fruit_id": 162646,
    "name": "mandarino siciliano",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-pesca_gialla_tree.png",
    "number": 1,
    "co2_unit": 0.06077,
    "farmer_name": "Azienda Agricola Parisi",
    "location_country": "Lentini, Sicilia",
    "location_province": "Lentini, Siracusa (SR)",
    "selected": true,
    "co2": "0,061"
  },
  {
    "fruit_id": 60809,
    "name": "mela grigia di torriana",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1583232953/fruits/biorfarm-icon-mela-grigia-di-torriana_tree.png",
    "number": 1,
    "co2_unit": 0.04,
    "farmer_name": "Famiglia Fraire",
    "location_country": "Barge, Piemonte",
    "location_province": "Barge, Cuneo (CN)",
    "selected": true,
    "co2": "0,040"
  },
  {
    "fruit_id": 63726,
    "name": "mela stark",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1586198673/fruits/biorfarm-icon-mela-stark_tree.png",
    "number": 1,
    "co2_unit": 0.04,
    "farmer_name": "Paolo Rossi",
    "location_country": "Clès, Trentino",
    "location_province": "Val di Non, Trento (TN)",
    "selected": true,
    "co2": "0,040"
  },
  {
    "fruit_id": 33357,
    "name": "melograno akko",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-melograno-akko_tree.png",
    "number": 1,
    "co2_unit": 0.05,
    "farmer_name": "Giuseppe Costantino",
    "location_country": "Alcamo, Sicilia",
    "location_province": "Alcamo, Trapani (TP)",
    "selected": true,
    "co2": "0,050"
  },
  {
    "fruit_id": 137391,
    "name": "pera carmosina",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1621957796/fruits/biorfarm-icon-pera-carmosina_tree.png",
    "number": 1,
    "co2_unit": 0.046,
    "farmer_name": "Famiglia Galante",
    "location_country": "Ginosa, Puglia",
    "location_province": "Ginosa, Taranto (TA)",
    "selected": true,
    "co2": "0,046"
  },
  {
    "fruit_id": 71557,
    "name": "pera williams",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-pera_williams_tree.png",
    "number": 1,
    "co2_unit": 0.046,
    "farmer_name": "Lorenzo Sacchetto",
    "location_country": "Lagnasco, Piemonte",
    "location_province": "Lagnasco, Cuneo (CN)",
    "selected": true,
    "co2": "0,046"
  },
  {
    "fruit_id": 17811,
    "name": "susina nera",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-susina_nera_tree.png",
    "number": 1,
    "co2_unit": 0.024,
    "farmer_name": "Daniele Bucci",
    "location_country": "Faenza, Emilia Romagna",
    "location_province": "Faenza, Ravenna (RA)",
    "selected": true,
    "co2": "0,024"
  },
  {
    "fruit_id": 78574,
    "name": "uva bianca moscato",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1599730568/fruits/biorfarm-icon-uva-bianca-moscato_tree.png",
    "number": 1,
    "co2_unit": 0.04,
    "farmer_name": "Fabio Proverbio",
    "location_country": "Treviglio, Lombardia",
    "location_province": "Treviglio, Bergamo (BG)",
    "selected": true,
    "co2": "0,040"
  },
  {
    "fruit_id": 32926,
    "name": "uva vittoria",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1591870312/fruits/biorfarm-icon-uva-bianca_tree.png",
    "number": 1,
    "co2_unit": 0.04,
    "farmer_name": "Antonia Giannella",
    "location_country": "San Ferdinando, Puglia",
    "location_province": "San Ferdinando di Puglia, Barletta-Andria-Trani (BT)",
    "selected": true,
    "co2": "0,040"
  }
];
