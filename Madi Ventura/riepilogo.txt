CO2 totali: 675 Kg

Agricoltori supportati: 15

Alberi in partnerships: 15

Codice Sconto: no

Page URL: https://www.biorfarm.com/madiventura/

Logo URL: https://res.cloudinary.com/biorfarm/image/upload/v1644511710/static/ventura-logo.svg

Banner URL: undefined

Slogan: "Madi Ventura si impegna da più di 80 anni a diffondere uno stile di vita sano attraverso l’alimentazione selezionando la migliore frutta secca ed essiccata da tutto il mondo. Con la creazione di questo frutteto abbiamo voluto valorizzare le culture ortofrutticole italiane e i loro custodi, offrendo i loro frutti a tutte quelle persone che in un modo o nell’altro sono vicine e collaborano con Ventura. Un piccolo gesto a favore dell’ambiente e della Bontà ed il Benessere con un forte impatto positivo."