CO2 totali: 7.000 Kg

Agricoltori supportati: 1

Alberi in partnerships: 100

Codice Sconto: targetitalia10

Page URL: https://www.biorfarm.com/targetitalia/

Logo URL: https://res.cloudinary.com/biorfarm/image/upload/v1608630073/static/target_italia-logo.png

Banner URL: https://res.cloudinary.com/biorfarm/image/upload/v1608630074/static/b2b-header-target_italia.png

Slogan: "Nella nostra agenzia di Trade Marketing crediamo nel potenziale delle persone: coltivare un rapporto di partnership e collaborazione è fondamentale per ottimizzare i processi e far crescere rigogliosi i progetti.

In quest’anno pieno di sfide e cambiamenti abbiamo scelto un regalo speciale, in linea con i nostri valori: adottare per un anno 100 alberi di Arancia Tarocco Gallo, creando un frutteto digitale biologico!

Una iniziativa non solo a sostegno dell’ambiente, ma anche delle piccole realtà economiche italiane.

Inoltre abbiamo pensato anche alla tua salute: inserendo il codice personale che ti abbiamo inviato via mail attiverai l’adozione del tuo albero e potrai ricevere direttamente al tuo domicilio 5 kg di arance: tanta vitamina C per iniziare il nuovo anno con la carica giusta!
 
Perché in Target Italia crediamo che non ci sia miglior momento del presente per investire nel futuro e vogliamo affrontare l'anno in arrivo con tutta l’energia e l’ottimismo che ci hanno sempre contraddistinto.

Il momento migliore per piantare un albero era 20 anni fa. Il secondo miglior momento è ora."