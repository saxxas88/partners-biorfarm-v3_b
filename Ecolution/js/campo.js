/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 61341,
    "name": "cachi di romagna",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1583752500/fruits/biorfarm-icon-caco-loto-di-romagna_tree.png",
    "number": 15,
    "co2_unit": 0.0206,
    "farmer_name": "Podere Poluzza Nuova",
    "location_country": "Imola, Emilia-Romagna",
    "location_province": "Imola, Bologna (BO)",
    "selected": true,
    "co2": "0,31"
  },
  {
    "fruit_id": 139232,
    "name": "noce essiccata",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1622821393/fruits/biorfarm-icon-noce-essiccata_tree.png",
    "number": 10,
    "co2_unit": 0.83943,
    "farmer_name": "Claudio Granelli",
    "location_country": "Varano de Melegari, Emilia-Romagna",
    "location_province": "Varano de Melegari, Parma (PR)",
    "selected": true,
    "co2": "8,39"
  },
  {
    "fruit_id": 17811,
    "name": "susina nera",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-susina_nera_tree.png",
    "number": 25,
    "co2_unit": 0.024,
    "farmer_name": "Daniele Bucci",
    "location_country": "Faenza, Emilia Romagna",
    "location_province": "Faenza, Ravenna (RA)",
    "selected": true,
    "co2": "0,60"
  }
]