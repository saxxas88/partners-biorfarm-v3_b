/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees=[
  
  {
    "fruit_id": 11526,
    "name": "mela gala",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mela_gala_tree.png",
    "number": 30,
    "co2_unit": 0.04,
    "farmer_name": "Fratelli Brero",
    "location_country": "Lagnasco, Piemonte",
    "location_province": "Lagnasco, Cuneo (CN)",
    "selected": true,
    "co2": "1,20"
  },
  {
    "fruit_id": 38775,
    "name": "limone di calabria",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-limone-zagara-bianca_tree.png",
    "number": 26,
    "co2_unit": 0.068,
    "farmer_name": "Giovanni Zappavigna",
    "location_country": "Ardore, Calabria",
    "location_province": "Ardore, Reggio Calabria (RC)",
    "selected": true,
    "co2": "1,77"
  },
  {
    "fruit_id": 74882,
    "name": "mandarino tardivo di ciaculli",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595409710/fruits/biorfarm-icon-mandarino-tardivo-di-ciaculli_tree.png",
    "number": 25,
    "co2_unit": 0.06077,
    "farmer_name": "Fratelli Marceno",
    "location_country": " Palermo, Sicilia",
    "location_province": "Palermo (PA)",
    "selected": true,
    "co2": "1,52"
  },
  {
    "fruit_id": 682,
    "name": "arancia navelina",
    "icon": "https://res.cloudinary.com/biorfarm/image/upload/v1561625041/fruits/biorfarm-icon-arancia_navelina_fruit.png",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477692/fruits/biorfarm-icon-arancia_navelina_tree.png",
    "img_tree": "https://www.biorfarm.com/wp-content/uploads/2020/03/Arance-Navel-di-Sicilia.jpg",
    "number": 25,
    "co2_unit": 0.07,
    "farmer_name": "Paolo De Falco",
    "location_country": "Corigliano-Rossano, Calabria",
    "location_province": "Piana di Sibari, Corigliano-Rossano (CS)",
    "selected": true,
    "co2": "1,75"
  },
  {
    "fruit_id": 84238,
    "name": "albicocca portici",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1604403563/fruits/biorfarm-icon-albicocca-portici-dell-orto_tree.png",
    "number": 2,
    "co2_unit": 0.07,
    "farmer_name": "Famiglia Dell'Orto",
    "location_country": "Piana del Sele, Campania",
    "location_province": "Piana del Sele, Salerno (SA)",
    "selected": true,
    "co2": "0,14"
  },
  {
    "fruit_id": 14648,
    "name": "arancia valencia",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477692/fruits/biorfarm-icon-arancia_valencia_tree.png",
    "number": 2,
    "co2_unit": 0.07,
    "farmer_name": "Famiglia Leone",
    "location_country": "Noto, Sicilia",
    "location_province": "Noto, Siracusa (SR)",
    "selected": true,
    "co2": "0,14"
  },
  {
    "fruit_id": 75223,
    "name": "arancia ovale",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595581789/fruits/biorfarm-icon-arancia-ovale_tree.png",
    "number": 1,
    "co2_unit": 0.07,
    "farmer_name": "Emanuele Calvo",
    "location_country": "Mineo, Sicilia",
    "location_province": "Mineo, Catania (CT)",
    "selected": true,
    "co2": "0,07"
  },
  {
    "fruit_id": 125431,
    "name": "ciliegia igp di marostica",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1617732978/fruits/biorfarm-icon-ciliegia-igp-di-marostica_tree.png",
    "number": 1,
    "co2_unit": 0.125,
    "farmer_name": "Riccardo Casarotto",
    "location_country": "San Giorgio di Perlena, Veneto",
    "location_province": "San Giorgio di Perlena, Vicenza (VI)",
    "selected": true,
    "co2": "0,13"
  },
  
  {
    "fruit_id": 60703,
    "name": "germogli di bambù",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1582824359/fruits/biorfarm-icon-germogli-di-bambu_tree.png",
    "number": 1,
    "co2_unit": 0.002,
    "farmer_name": "Famiglia Ferri",
    "location_country": "Castel Rozzone, Lombardia",
    "location_province": "Castel Rozzone, Bergamo (BG)",
    "selected": true,
    "co2": "0,00"
  },
  {
    "fruit_id": 114603,
    "name": "mela gala casarotti",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mela_gala_tree.png",
    "number": 1,
    "co2_unit": 0.04,
    "farmer_name": "Az. Agricola Famiglia Casarotti",
    "location_country": "Caldiero, Veneto",
    "location_province": "Caldiero, Verona (VR)",
    "selected": true,
    "co2": "0,04"
  },
  {
    "fruit_id": 72045,
    "name": "nocciola tonda gentile igp in guscio",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595002902/fruits/biorfarm-icon-nocciola-tonda-gentile-igp_tree.png",
    "number": 1,
    "co2_unit": 0.01799,
    "farmer_name": "Naturalmente",
    "location_country": "Murozzano, Piemonte",
    "location_province": "Murazzano, Cuneo (CN)",
    "selected": true,
    "co2": "0,02"
  },
  {
    "fruit_id": 76304,
    "name": "pesca gialla toscana",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-pesca_gialla_tree.png",
    "number": 1,
    "co2_unit": 0.063,
    "farmer_name": "Famiglia Checcucci",
    "location_country": "Roccastrada, Toscana",
    "location_province": "Roccastrada, Grosseto (GR)",
    "selected": true,
    "co2": "0,06"
  },
  {
    "fruit_id": 63670,
    "name": "susina sorriso di primavera",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1586198672/fruits/biorfarm-icon-susina-sorriso-di-primavera_tree.png",
    "number": 1,
    "co2_unit": 0.024,
    "farmer_name": "Famiglia Dell'Orto",
    "location_country": "Piana del Sele, Campania",
    "location_province": "Piana del Sele, Salerno (SA)",
    "selected": true,
    "co2": "0,02"
  },
  {
    "fruit_id": 119195,
    "name": "farina tipo 1 di grano antico gentil tramazzo",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1614016418/fruits/biorfarm-icon-farina-tipo1-grano-antico-gentil-tramazzo_tree.png",
    "number": 1,
    "co2_unit": 0,
    "farmer_name": "Francesco Mazzi",
    "location_country": "Tredozio, Emilia-Romagna",
    "location_province": "Tredozio, Forlì-Cesena (FC)",
    "selected": true,
    "co2": "0,00"
  },
  {
    "fruit_id": 119637,
    "name": "alveare acacia",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1607338355/fruits/biorfarm-icon-alveare-acacia_tree.png",
    "number": 1,
    "co2_unit": 0,
    "farmer_name": "Elio Varni",
    "location_country": "Genova, Liguria",
    "location_province": "Genova (GE)",
    "selected": true,
    "co2": "0,00"
  },
  {
    "fruit_id": 119864,
    "name": "alveare millefiori",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1607338355/fruits/biorfarm-icon-alveare-millefiori_tree.png",
    "number": 1,
    "co2_unit": 0,
    "farmer_name": "Marcoraniero Melani",
    "location_country": "Collegiove, Lazio",
    "location_province": "Collegiove, Rieti (RI)",
    "selected": true,
    "co2": "0,00"
  }
   ]