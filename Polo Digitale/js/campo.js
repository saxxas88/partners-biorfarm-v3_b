/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = 
[
  {
    "fruit_id": 75554,
    "name": "arancia biondo di caulonia",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595943654/fruits/biorfarm-icon-arancia-biondo-di-caulonia_tree.png",
    "number": 1,
    "co2_unit": 0.07,
    "farmer_name": "Ilaria Campisi",
    "location_country": "Caulonia, Calabria",
    "location_province": "Caulonia, Reggio Calabria (RC)",
    "selected": true,
    "co2": "0,07"
  },
  {
    "fruit_id": 129165,
    "name": "arancia tarocco",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-arancia-tarocco_tree.png",
    "number": 1,
    "co2_unit": 0.07,
    "farmer_name": "Angela Arcoria",
    "location_country": "Paternò, Sicilia",
    "location_province": "Paternò, Catania (CT)",
    "selected": true,
    "co2": "0,07"
  },
  {
    "fruit_id": 75225,
    "name": "arancia tarocco sant'alfio",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595581977/fruits/biorfarm-icon-arancia-tarocco-sant-alfio_tree.png",
    "number": 1,
    "co2_unit": 0.07,
    "farmer_name": "Emanuele Calvo",
    "location_country": "Mineo, Sicilia",
    "location_province": "Mineo, Catania (CT)",
    "selected": true,
    "co2": "0,07"
  },
  {
    "fruit_id": 62374,
    "name": "kiwi hayward",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1581586234/fruits/biorfarm-icon-kiwi_tree.png",
    "number": 1,
    "co2_unit": 0.00115,
    "farmer_name": "Azienda Agricola Masseria Cesaro",
    "location_country": "Teverola, Campania",
    "location_province": "Teverola, Caserta (CE)",
    "selected": true,
    "co2": "0,00"
  },
  {
    "fruit_id": 167874,
    "name": "mandarino della riviera dei gelsomini",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1638871720/fruits/biorfarm-icon-mandarino_tree.png",
    "number": 1,
    "co2_unit": 0.06077,
    "farmer_name": "Giovanni Zappavigna",
    "location_country": "Ardore, Calabria",
    "location_province": "Ardore, Reggio Calabria (RC)",
    "selected": true,
    "co2": "0,06"
  },
  {
    "fruit_id": 127329,
    "name": "mela golden delicious",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mela_golden_tree.png",
    "number": 1,
    "co2_unit": 0.04,
    "farmer_name": "Mirko Bezzi",
    "location_country": "Urbana, Veneto",
    "location_province": "Urbana, Padova (PD)",
    "selected": true,
    "co2": "0,04"
  },
  {
    "fruit_id": 72045,
    "name": "nocciola tonda gentile igp in guscio",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595002902/fruits/biorfarm-icon-nocciola-tonda-gentile-igp_tree.png",
    "number": 1,
    "co2_unit": 0.01799,
    "farmer_name": "Naturalmente",
    "location_country": "Murozzano, Piemonte",
    "location_province": "Murazzano, Cuneo (CN)",
    "selected": true,
    "co2": "0,02"
  },
  {
    "fruit_id": 71557,
    "name": "pera williams",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-pera_williams_tree.png",
    "number": 1,
    "co2_unit": 0.046,
    "farmer_name": "Lorenzo Sacchetto",
    "location_country": "Lagnasco, Piemonte",
    "location_province": "Lagnasco, Cuneo (CN)",
    "selected": true,
    "co2": "0,05"
  },
  {
    "fruit_id": 17811,
    "name": "susina nera",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-susina_nera_tree.png",
    "number": 1,
    "co2_unit": 0.024,
    "farmer_name": "Daniele Bucci",
    "location_country": "Faenza, Emilia Romagna",
    "location_province": "Faenza, Ravenna (RA)",
    "selected": true,
    "co2": "0,02"
  },
  {
    "fruit_id": 139519,
    "name": "susina santa clara",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1623063552/fruits/biorfarm-icon-susina-santa-clara_tree.png",
    "number": 1,
    "co2_unit": 0.024,
    "farmer_name": "Famiglia Banchio",
    "location_country": "Saluzzo, Piemonte",
    "location_province": "Saluzzo, Cuneo (CN)",
    "selected": true,
    "co2": "0,02"
  },
  {
    "fruit_id": 63670,
    "name": "susina sorriso di primavera",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1586198672/fruits/biorfarm-icon-susina-sorriso-di-primavera_tree.png",
    "number": 1,
    "co2_unit": 0.024,
    "farmer_name": "Famiglia Dell'Orto",
    "location_country": "Piana del Sele, Campania",
    "location_province": "Piana del Sele, Salerno (SA)",
    "selected": true,
    "co2": "0,02"
  },
  {
    "fruit_id": 78574,
    "name": "uva bianca moscato",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1599730568/fruits/biorfarm-icon-uva-bianca-moscato_tree.png",
    "number": 1,
    "co2_unit": 0.04,
    "farmer_name": "Fabio Proverbio",
    "location_country": "Treviglio, Lombardia",
    "location_province": "Treviglio, Bergamo (BG)",
    "selected": true,
    "co2": "0,04"
  }
]