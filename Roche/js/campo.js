/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
    {
      "fruit_id": 89695,
      "name": "alveare castagno",
      "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1606987616/fruits/biorfarm-icon-alveare-castagno_tree.png",
      "number": 60,
      "co2_unit": 0,
      "farmer_name": "Famiglia Nucci",
      "location_country": "Santarcangelo di Romagna, Emilia-Romagna",
      "location_province": "Santarcangelo di Romagna, Rimini (RN)",
      "selected": true,
      "co2": "0,00"
    },
    {
      "fruit_id": 117881,
      "name": "filare sangiovese – vino litiano maremma toscana doc",
      "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1612950789/fruits/biorfarm-icon-filare-litiano-sangiovese-maremma-doc_tree.png",
      "number": 60,
      "co2_unit": 0,
      "farmer_name": "Famiglia Checcucci",
      "location_country": "Roccastrada, Toscana",
      "location_province": "Roccastrada, Grosseto (GR)",
      "selected": true,
      "co2": "0,00"
    },
    {
      "fruit_id": 225111,
      "name": "olivo abruzzese",
      "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-olivo_tree.png",
      "number": 60,
      "co2_unit": 0.3,
      "farmer_name": "Famiglia Montecchia",
      "location_country": "Morro D'Oro, Abruzzo",
      "location_province": "Morro D'Oro,Teramo (TE)",
      "selected": true,
      "co2": "18,00"
    }
  ]