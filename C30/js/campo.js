/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 682,
    "name": "arancia navelina",
    "icon": "https://res.cloudinary.com/biorfarm/image/upload/v1561625041/fruits/biorfarm-icon-arancia_navelina_fruit.png",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477692/fruits/biorfarm-icon-arancia_navelina_tree.png",
    "img_tree": "https://www.biorfarm.com/wp-content/uploads/2020/03/Arance-Navel-di-Sicilia.jpg",
    "number": 1,
    "co2_unit": 0.07,
    "farmer_name": "Paolo De Falco",
    "location_country": "Corigliano-Rossano, Calabria",
    "location_province": "Piana di Sibari, Corigliano-Rossano (CS)",
    "selected": true,
    "co2": "0,07"
  },
  {
    "fruit_id": 74881,
    "name": "mandarino avana",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595409639/fruits/biorfarm-icon-mandarino-avana_tree.png",
    "number": 1,
    "co2_unit": 0.06077,
    "farmer_name": "Fratelli Marceno",
    "location_country": " Palermo, Sicilia",
    "location_province": "Palermo (PA)",
    "selected": true,
    "co2": "0,06"
  },
  {
    "fruit_id": 114606,
    "name": "mela fujion",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1610988794/fruits/biorfarm-icon-mela_fujion_tree.png",
    "number": 1,
    "co2_unit": 0.04,
    "farmer_name": "Az. Agricola Famiglia Casarotti",
    "location_country": "Caldiero, Veneto",
    "location_province": "Caldiero, Verona (VR)",
    "selected": true,
    "co2": "0,04"
  },
  {
    "fruit_id": 114603,
    "name": "mela gala casarotti",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mela_gala_tree.png",
    "number": 1,
    "co2_unit": 0.04,
    "farmer_name": "Az. Agricola Famiglia Casarotti",
    "location_country": "Caldiero, Veneto",
    "location_province": "Caldiero, Verona (VR)",
    "selected": true,
    "co2": "0,04"
  },
  {
    "fruit_id": 127213,
    "name": "pera williams",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-pera_williams_tree.png",
    "number": 1,
    "co2_unit": 0.046,
    "farmer_name": "Mirko Bezzi",
    "location_country": "Urbana, Veneto",
    "location_province": "Urbana, Padova (PD)",
    "selected": true,
    "co2": "0,05"
  },
  {
    "fruit_id": 139519,
    "name": "susina santa clara",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1623063552/fruits/biorfarm-icon-susina-santa-clara_tree.png",
    "number": 1,
    "co2_unit": 0.024,
    "farmer_name": "Famiglia Banchio",
    "location_country": "Saluzzo, Piemonte",
    "location_province": "Saluzzo, Cuneo (CN)",
    "selected": true,
    "co2": "0,02"
  }
]