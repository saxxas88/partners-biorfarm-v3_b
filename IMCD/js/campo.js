/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees=[
  {
    "fruit_id": 188325,
    "name": "albicocca rossa tardiva",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561625041/fruits/biorfarm-icon-albicocca_rossa_fruit.png",
    "number": 1,
    "co2_unit": 0.07,
    "farmer_name": "Famiglia Dell'Orto",
    "location_country": "Piana del Sele, Campania",
    "location_province": "Piana del Sele, Salerno (SA)",
    "selected": true,
    "co2": "0,070"
  },
  {
    "fruit_id": 167651,
    "name": "arancia biondo della riviera dei gelsomini",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1639568082/fruits/biorfarm-icon-arancia-biondo_tree.png",
    "number": 4,
    "co2_unit": 0.07,
    "farmer_name": "Giovanni Zappavigna",
    "location_country": "Ardore, Calabria",
    "location_province": "Ardore, Reggio Calabria (RC)",
    "selected": true,
    "co2": "0,280"
  },
  {
    "fruit_id": 234528,
    "name": "clementina calabrese",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-clementina_tree.png",
    "number": 1,
    "co2_unit": 0.06,
    "farmer_name": "Az. Agricola Paolo Maria Lamenza",
    "location_country": "Corigliano-Rossano, Calabria",
    "location_province": "Corigliano-Rossano, Cosenza (CS)",
    "selected": true,
    "co2": "0,060"
  },
  {
    "fruit_id": 61443,
    "name": "kiwi boerica",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1581586234/fruits/biorfarm-icon-kiwi_tree.png",
    "number": 1,
    "co2_unit": 0.00115,
    "farmer_name": "Azienda Agricola Cordopatri",
    "location_country": "Taurianova, Calabria",
    "location_province": "Taurianova, Reggio Calabria (RC)",
    "selected": true,
    "co2": "0,001"
  },
  {
    "fruit_id": 127329,
    "name": "mela golden delicious",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mela_golden_tree.png",
    "number": 1,
    "co2_unit": 0.04,
    "farmer_name": "Mirko Bezzi",
    "location_country": "Urbana, Veneto",
    "location_province": "Urbana, Padova (PD)",
    "selected": true,
    "co2": "0,040"
  },
  {
    "fruit_id": 33357,
    "name": "melograno akko",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-melograno-akko_tree.png",
    "number": 1,
    "co2_unit": 0.05,
    "farmer_name": "Giuseppe Costantino",
    "location_country": "Alcamo, Sicilia",
    "location_province": "Alcamo, Trapani (TP)",
    "selected": true,
    "co2": "0,050"
  },
  {
    "fruit_id": 71557,
    "name": "pera williams",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-pera_williams_tree.png",
    "number": 1,
    "co2_unit": 0.046,
    "farmer_name": "Lorenzo Sacchetto",
    "location_country": "Lagnasco, Piemonte",
    "location_province": "Lagnasco, Cuneo (CN)",
    "selected": true,
    "co2": "0,046"
  },
  {
    "fruit_id": 135921,
    "name": "pesca royal summer",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1620376201/fruits/biorfarm-icon-pesca-royal-summer_tree.png",
    "number": 1,
    "co2_unit": 0.063,
    "farmer_name": "Famiglia Scardovi",
    "location_country": "Faenza, Emilia Romagna",
    "location_province": "Faenza, Ravenna (RA)",
    "selected": true,
    "co2": "0,063"
  },
  {
    "fruit_id": 139519,
    "name": "susina santa clara",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1623063552/fruits/biorfarm-icon-susina-santa-clara_tree.png",
    "number": 1,
    "co2_unit": 0.024,
    "farmer_name": "Famiglia Banchio",
    "location_country": "Saluzzo, Piemonte",
    "location_province": "Saluzzo, Cuneo (CN)",
    "selected": true,
    "co2": "0,024"
  },
  {
    "fruit_id": 137216,
    "name": "uva italia",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1591870312/fruits/biorfarm-icon-uva-bianca_tree.png",
    "number": 1,
    "co2_unit": 0.04,
    "farmer_name": "Famiglia Losito",
    "location_country": "Rutigliano, Puglia",
    "location_province": "Rutigliano, Bari (BA)",
    "selected": true,
    "co2": "0,040"
  }
]