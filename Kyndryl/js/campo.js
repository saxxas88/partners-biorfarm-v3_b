/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 123965,
    "name": "arancia sanguinello",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-arancia-sanguinello_tree.png",
    "number": 300,
    "co2_unit": 0.07,
    "farmer_name": "Carlo Marilli",
    "location_country": "Carlentini, Sicilia",
    "location_province": "Carlentini, Siracusa (SR)",
    "selected": true,
    "co2": "21,00"
  },
  {
    "fruit_id": 114603,
    "name": "mela gala casarotti",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mela_gala_tree.png",
    "number": 140,
    "co2_unit": 0.04,
    "farmer_name": "Az. Agricola Famiglia Casarotti",
    "location_country": "Caldiero, Veneto",
    "location_province": "Caldiero, Verona (VR)",
    "selected": true,
    "co2": "5,60"
  }
]