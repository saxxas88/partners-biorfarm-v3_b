/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees= [
  {
    "fruit_id": 188235,
    "name": "albicocca vesuviana",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477692/fruits/biorfarm-icon-albicocca-pellecchiella_tree.png",
    "number": 50,
    "co2_unit": 0.07,
    "farmer_name": "Famiglia Dell'Orto",
    "location_country": "Piana del Sele, Campania",
    "location_province": "Piana del Sele, Salerno (SA)",
    "selected": true,
    "co2": "3,50"
  },
  {
    "fruit_id": 167651,
    "name": "arancia biondo della riviera dei gelsomini",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1639568082/fruits/biorfarm-icon-arancia-biondo_tree.png",
    "number": 75,
    "co2_unit": 0.07,
    "farmer_name": "Giovanni Zappavigna",
    "location_country": "Ardore, Calabria",
    "location_province": "Ardore, Reggio Calabria (RC)",
    "selected": true,
    "co2": "5,25"
  },
  {
    "fruit_id": 14647,
    "name": "carruba",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477692/fruits/biorfarm-icon-carruba_tree.png",
    "number": 20,
    "co2_unit": 0.3,
    "farmer_name": "Famiglia Leone",
    "location_country": "Noto, Sicilia",
    "location_province": "Noto, Siracusa (SR)",
    "selected": true,
    "co2": "6,00"
  },
  {
    "fruit_id": 234528,
    "name": "clementina calabrese",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-clementina_tree.png",
    "number": 50,
    "co2_unit": 0.06,
    "farmer_name": "Az. Agricola Paolo Maria Lamenza",
    "location_country": "Corigliano-Rossano, Calabria",
    "location_province": "Corigliano-Rossano, Cosenza (CS)",
    "selected": true,
    "co2": "3,00"
  },
  {
    "fruit_id": 74881,
    "name": "mandarino avana",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595409639/fruits/biorfarm-icon-mandarino-avana_tree.png",
    "number": 75,
    "co2_unit": 0.06077,
    "farmer_name": "Fratelli Marceno",
    "location_country": " Palermo, Sicilia",
    "location_province": "Palermo (PA)",
    "selected": true,
    "co2": "4,56"
  },
  {
    "fruit_id": 198859,
    "name": "mango kensington",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-mango-keitt_tree.png",
    "number": 30,
    "co2_unit": 0.01015,
    "farmer_name": "Katia Scianna",
    "location_country": "Balestrate, Sicilia",
    "location_province": "Balestrate, Palermo (PA)",
    "selected": true,
    "co2": "0,30"
  },
  {
    "fruit_id": 63726,
    "name": "mela stark",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1586198673/fruits/biorfarm-icon-mela-stark_tree.png",
    "number": 50,
    "co2_unit": 0.04,
    "farmer_name": "Paolo Rossi",
    "location_country": "Clès, Trentino",
    "location_province": "Val di Non, Trento (TN)",
    "selected": true,
    "co2": "2,00"
  },
  {
    "fruit_id": 127283,
    "name": "pera santa maria",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1618823679/fruits/biorfarm-icon-pera-santa-maria_tree.png",
    "number": 20,
    "co2_unit": 0.046,
    "farmer_name": "Mirko Bezzi",
    "location_country": "Urbana, Veneto",
    "location_province": "Urbana, Padova (PD)",
    "selected": true,
    "co2": "0,92"
  },
  {
    "fruit_id": 70009,
    "name": "uva crimson rossa",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1591869855/fruits/biorfarm-icon-uva-crimson-rossa_tree.png",
    "number": 50,
    "co2_unit": 0.04,
    "farmer_name": "Famiglia Galante",
    "location_country": "Ginosa, Puglia",
    "location_province": "Ginosa, Taranto (TA)",
    "selected": true,
    "co2": "2,00"
  }
]