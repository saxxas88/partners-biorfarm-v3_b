/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 155688,
    "name": "fragola siciliana",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1618389350/fruits/biorfarm-icon-fragola-siciliana_tree.png",
    "number": 100,
    "co2_unit": 0.00011,
    "farmer_name": "Filippo Licari",
    "location_country": "Marsala, Sicilia",
    "location_province": "Marsala, Trapani (TP)",
    "selected": true,
    "co2": "0,01"
  },
  {
    "fruit_id": 62374,
    "name": "kiwi hayward",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1581586234/fruits/biorfarm-icon-kiwi_tree.png",
    "number": 100,
    "co2_unit": 0.00115,
    "farmer_name": "Azienda Agricola Masseria Cesaro",
    "location_country": "Teverola, Campania",
    "location_province": "Teverola, Caserta (CE)",
    "selected": true,
    "co2": "0,11"
  },
  {
    "fruit_id": 681,
    "name": "mela fuji",
    "icon": "https://res.cloudinary.com/biorfarm/image/upload/v1561625042/fruits/biorfarm-icon-mela_fuji_fruit.png",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mela_fuji_tree.png",
    "img_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1500289952/fruits/mela_fuji_biorfarm_frutta.jpg",
    "number": 100,
    "co2_unit": 0.04,
    "farmer_name": "Paolo Rossi",
    "location_country": "Clès, Trentino",
    "location_province": "Val di Non, Clès (TN)",
    "selected": true,
    "co2": "4,00"
  },
  {
    "fruit_id": 678,
    "name": "pera buona luisa",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-pera_buona_luisa_tree.png",
    "number": 100,
    "co2_unit": 0.046,
    "farmer_name": "Paolo Rossi",
    "location_country": "Clès, Trentino",
    "location_province": "Val di Non, Trento (TN)",
    "selected": true,
    "co2": "4,60"
  },
  {
    "fruit_id": 156781,
    "name": "pesca royal summer",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-pesca_gialla_tree.png",
    "number": 100,
    "co2_unit": 0.063,
    "farmer_name": "Podere Poluzza Nuova",
    "location_country": "Imola, Emilia Romagna",
    "location_province": "Imola, Bologna (BO)",
    "selected": true,
    "co2": "6,30"
  }
]