/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 76253,
    "name": "albicocca portici",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1592238079/fruits/biorfarm-icon-albicocca-portici_tree.png",
    "number": 4,
    "co2_unit": 0.07,
    "farmer_name": "Emilia Martino",
    "location_country": "Campobasso, Molise",
    "location_province": "Campobasso (CB)",
    "selected": true,
    "co2": "0,280"
  },
  {
    "fruit_id": 162058,
    "name": "arancia rossa di sicilia IGP",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1638871720/fruits/biorfarm-icon-arancia-rossa_tree.png",
    "number": 4,
    "co2_unit": 0.07,
    "farmer_name": "Azienda Agricola Parisi",
    "location_country": "Lentini, Sicilia",
    "location_province": "Lentini, Siracusa (SR)",
    "selected": true,
    "co2": "0,280"
  },
  {
    "fruit_id": 129181,
    "name": "arancia valencia midknight",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477692/fruits/biorfarm-icon-arancia_valencia_tree.png",
    "number": 4,
    "co2_unit": 0.07,
    "farmer_name": "Angela Arcoria",
    "location_country": "Paternò, Sicilia",
    "location_province": "Paternò, Catania (CT)",
    "selected": true,
    "co2": "0,280"
  },
  {
    "fruit_id": 683,
    "name": "clementina calabrese",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-clementina_tree.png",
    "number": 4,
    "co2_unit": 0.06,
    "farmer_name": "Paolo De Falco",
    "location_country": "Corigliano-Rossano, Calabria",
    "location_province": "Piana di Sibari, Corigliano-Rossano (CS)",
    "selected": true,
    "co2": "0,240"
  },
  {
    "fruit_id": 166373,
    "name": "fragola veneta",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1591263270/fruits/biorfarm-icon-fragola_tree.png",
    "number": 4,
    "co2_unit": 0.00084,
    "farmer_name": "Azienda Agricola Fiorio",
    "location_country": "Roverchiara, Veneto",
    "location_province": "Roverchiara, Verona (VR)",
    "selected": true,
    "co2": "0,003"
  },
  {
    "fruit_id": 62374,
    "name": "kiwi hayward",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1581586234/fruits/biorfarm-icon-kiwi_tree.png",
    "number": 4,
    "co2_unit": 0.00115,
    "farmer_name": "Azienda Agricola Masseria Cesaro",
    "location_country": "Teverola, Campania",
    "location_province": "Teverola, Caserta (CE)",
    "selected": true,
    "co2": "0,005"
  },
  {
    "fruit_id": 114603,
    "name": "mela gala casarotti",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mela_gala_tree.png",
    "number": 4,
    "co2_unit": 0.04,
    "farmer_name": "Az. Agricola Famiglia Casarotti",
    "location_country": "Caldiero, Veneto",
    "location_province": "Caldiero, Verona (VR)",
    "selected": true,
    "co2": "0,160"
  },
  {
    "fruit_id": 60809,
    "name": "mela grigia di torriana",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1583232953/fruits/biorfarm-icon-mela-grigia-di-torriana_tree.png",
    "number": 4,
    "co2_unit": 0.04,
    "farmer_name": "Famiglia Fraire",
    "location_country": "Barge, Piemonte",
    "location_province": "Barge, Cuneo (CN)",
    "selected": true,
    "co2": "0,160"
  },
  {
    "fruit_id": 17812,
    "name": "nettarina di faenza",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-nettarina_tree.png",
    "number": 4,
    "co2_unit": 0.089,
    "farmer_name": "Daniele Bucci",
    "location_country": "Faenza, Emilia Romagna",
    "location_province": "Faenza, Ravenna (RA)",
    "selected": true,
    "co2": "0,356"
  },
  {
    "fruit_id": 678,
    "name": "pera buona luisa",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-pera_buona_luisa_tree.png",
    "number": 4,
    "co2_unit": 0.046,
    "farmer_name": "Paolo Rossi",
    "location_country": "Clès, Trentino",
    "location_province": "Val di Non, Trento (TN)",
    "selected": true,
    "co2": "0,184"
  },
  {
    "fruit_id": 119864,
    "name": "alveare millefiori",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1607338355/fruits/biorfarm-icon-alveare-millefiori_tree.png",
    "number": 2,
    "co2_unit": 0,
    "farmer_name": "Marcoraniero Melani",
    "location_country": "Collegiove, Lazio",
    "location_province": "Collegiove, Rieti (RI)",
    "selected": true,
    "co2": "0,000"
  }
]
