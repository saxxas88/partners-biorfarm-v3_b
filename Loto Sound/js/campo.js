/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 116257,
    "name": "albicocca rossa",
    "icon": "https://res.cloudinary.com/biorfarm/image/upload/v1561625041/fruits/biorfarm-icon-albicocca_rossa_fruit.png",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-albicocca-rossa_tree.png",
    "img_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1562324133/fruits/albicocca_rossa_biorfarm_frutta.jpg",
    "number": 1,
    "co2_unit": 0.07,
    "farmer_name": "Famiglia Dell'Orto",
    "location_country": "Piana del Sele, Campania",
    "location_province": "Piana del Sele, Salerno (SA)",
    "selected": true,
    "co2": "0,07"
  },
  {
    "fruit_id": 123917,
    "name": "arancia moro",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-arancia-moro_tree.png",
    "number": 1,
    "co2_unit": 0.07,
    "farmer_name": "Carlo Marilli",
    "location_country": "Carlentini, Sicilia",
    "location_province": "Carlentini, Siracusa (SR)",
    "selected": true,
    "co2": "0,07"
  },
  {
    "fruit_id": 125431,
    "name": "ciliegia igp di marostica",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1617732978/fruits/biorfarm-icon-ciliegia-igp-di-marostica_tree.png",
    "number": 1,
    "co2_unit": 0.125,
    "farmer_name": "Riccardo Casarotto",
    "location_country": "San Giorgio di Perlena, Veneto",
    "location_province": "San Giorgio di Perlena, Vicenza (VI)",
    "selected": true,
    "co2": "0,13"
  }
]
