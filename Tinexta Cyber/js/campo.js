/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 123917,
    "name": "arancia moro",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-arancia-moro_tree.png",
    "number": 10,
    "co2_unit": 0.07,
    "farmer_name": "Carlo Marilli",
    "location_country": "Carlentini, Sicilia",
    "location_province": "Carlentini, Siracusa (SR)",
    "selected": true,
    "co2": "0,70"
  },
  {
    "fruit_id": 683,
    "name": "clementina calabrese",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-clementina_tree.png",
    "number": 10,
    "co2_unit": 0.06,
    "farmer_name": "Paolo De Falco",
    "location_country": "Corigliano-Rossano, Calabria",
    "location_province": "Piana di Sibari, Corigliano-Rossano (CS)",
    "selected": true,
    "co2": "0,60"
  },
  {
    "fruit_id": 38775,
    "name": "limone di calabria",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-limone-zagara-bianca_tree.png",
    "number": 10,
    "co2_unit": 0.068,
    "farmer_name": "Giovanni Zappavigna",
    "location_country": "Ardore, Calabria",
    "location_province": "Ardore, Reggio Calabria (RC)",
    "selected": true,
    "co2": "0,68"
  },
  {
    "fruit_id": 74881,
    "name": "mandarino avana",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595409639/fruits/biorfarm-icon-mandarino-avana_tree.png",
    "number": 10,
    "co2_unit": 0.06077,
    "farmer_name": "Fratelli Marceno",
    "location_country": " Palermo, Sicilia",
    "location_province": "Palermo (PA)",
    "selected": true,
    "co2": "0,61"
  },
  {
    "fruit_id": 127329,
    "name": "mela golden delicious",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mela_golden_tree.png",
    "number": 10,
    "co2_unit": 0.04,
    "farmer_name": "Mirko Bezzi",
    "location_country": "Urbana, Veneto",
    "location_province": "Urbana, Padova (PD)",
    "selected": true,
    "co2": "0,40"
  },
  {
    "fruit_id": 156501,
    "name": "mela morgenduft dallago",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1637681977/fruits/biorfarm-icon-mela-morgenduft-dallago_tree.png",
    "number": 10,
    "co2_unit": 0.04,
    "farmer_name": "Diego Boggian",
    "location_country": "Legnago, Veneto",
    "location_province": "Legnago, Verona (VR)",
    "selected": true,
    "co2": "0,40"
  }
]