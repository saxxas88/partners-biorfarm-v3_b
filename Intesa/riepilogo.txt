CO2 totali: 121.200 Kg

Agricoltori supportati: 5

Alberi in partnerships: 2.000

Codice Sconto: INTESA10

Page URL: https://www.biorfarm.com/intesasanpaoloreward/

Logo URL: https://res.cloudinary.com/biorfarm/image/upload/v1620138973/static/intesa_sanpaolo-logo.png

Banner URL: https://res.cloudinary.com/biorfarm/image/upload/v1618248113/static/b2b-header-intesa.png

Slogan: "Fai la tua parte per un ambiente migliore
Grazie a Intesa Sanpaolo Reward puoi diventare promotore di un progetto sostenibile.
Entra a far parte di un’importante e significativa azienda agricola virtuale.
Con il Frutteto Reward puoi dare una nuova vita ad un albero, seguirne la crescita, la cura e le fasi di coltivazione biologica!"