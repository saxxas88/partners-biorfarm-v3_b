/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees=[
  {
    "fruit_id": 123965,
    "name": "arancia sanguinello",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-arancia-sanguinello_tree.png",
    "number": 7,
    "co2_unit": 0.07,
    "farmer_name": "Carlo Marilli",
    "location_country": "Carlentini, Sicilia",
    "location_province": "Carlentini, Siracusa (SR)",
    "selected": true,
    "co2": "0,49"
  },
  {
    "fruit_id": 129165,
    "name": "arancia tarocco",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-arancia-tarocco_tree.png",
    "number": 7,
    "co2_unit": 0.07,
    "farmer_name": "Angela Arcoria",
    "location_country": "Paternò, Sicilia",
    "location_province": "Paternò, Catania (CT)",
    "selected": true,
    "co2": "0,49"
  },
  {
    "fruit_id": 683,
    "name": "clementina calabrese",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-clementina_tree.png",
    "number": 7,
    "co2_unit": 0.06,
    "farmer_name": "Paolo De Falco",
    "location_country": "Corigliano-Rossano, Calabria",
    "location_province": "Piana di Sibari, Corigliano-Rossano (CS)",
    "selected": true,
    "co2": "0,42"
  },
  {
    "fruit_id": 38775,
    "name": "limone di calabria",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-limone-zagara-bianca_tree.png",
    "number": 7,
    "co2_unit": 0.068,
    "farmer_name": "Giovanni Zappavigna",
    "location_country": "Ardore, Calabria",
    "location_province": "Ardore, Reggio Calabria (RC)",
    "selected": true,
    "co2": "0,48"
  },
  {
    "fruit_id": 120232,
    "name": "limone verdello di siracusa igp",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1572517416/fruits/biorfarm-icon-limone_verdello_tree.png",
    "number": 7,
    "co2_unit": 0.068,
    "farmer_name": "Famiglia Campisi",
    "location_country": "Cassibile, Sicilia",
    "location_province": "Cassibile, Siracusa (SR)",
    "selected": true,
    "co2": "0,48"
  },
  {
    "fruit_id": 114603,
    "name": "mela gala casarotti",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mela_gala_tree.png",
    "number": 5,
    "co2_unit": 0.04,
    "farmer_name": "Az. Agricola Famiglia Casarotti",
    "location_country": "Caldiero, Veneto",
    "location_province": "Caldiero, Verona (VR)",
    "selected": true,
    "co2": "0,20"
  },
  {
    "fruit_id": 127329,
    "name": "mela golden delicious",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-mela_golden_tree.png",
    "number": 5,
    "co2_unit": 0.04,
    "farmer_name": "Mirko Bezzi",
    "location_country": "Urbana, Veneto",
    "location_province": "Urbana, Padova (PD)",
    "selected": true,
    "co2": "0,20"
  },
  {
    "fruit_id": 72045,
    "name": "nocciola tonda gentile igp in guscio",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595002902/fruits/biorfarm-icon-nocciola-tonda-gentile-igp_tree.png",
    "number": 5,
    "co2_unit": 0.01799,
    "farmer_name": "Naturalmente",
    "location_country": "Murozzano, Piemonte",
    "location_province": "Murazzano, Cuneo (CN)",
    "selected": true,
    "co2": "0,09"
  },
  {
    "fruit_id": 127283,
    "name": "pera santa maria",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1618823679/fruits/biorfarm-icon-pera-santa-maria_tree.png",
    "number": 5,
    "co2_unit": 0.046,
    "farmer_name": "Mirko Bezzi",
    "location_country": "Urbana, Veneto",
    "location_province": "Urbana, Padova (PD)",
    "selected": true,
    "co2": "0,23"
  },
  {
    "fruit_id": 121753,
    "name": "pompelmo rosa",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1615544540/fruits/biorfarm-icon-pompelmo-rosa_tree.png",
    "number": 5,
    "co2_unit": 0.0906,
    "farmer_name": "Cooperativa sociale ONLUS Si Può Fare",
    "location_country": "Noto, Sicilia",
    "location_province": "Noto, Siracusa (SR)",
    "selected": true,
    "co2": "0,45"
  },
  {
    "fruit_id": 78574,
    "name": "uva bianca moscato",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1599730568/fruits/biorfarm-icon-uva-bianca-moscato_tree.png",
    "number": 5,
    "co2_unit": 0.04,
    "farmer_name": "Fabio Proverbio",
    "location_country": "Treviglio, Lombardia",
    "location_province": "Treviglio, Bergamo (BG)",
    "selected": true,
    "co2": "0,20"
  }
]