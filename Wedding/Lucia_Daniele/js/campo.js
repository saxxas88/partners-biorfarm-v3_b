/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 116257,
    "name": "albicocca rossa",
    "icon": "https://res.cloudinary.com/biorfarm/image/upload/v1561625041/fruits/biorfarm-icon-albicocca_rossa_fruit.png",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561478223/fruits/biorfarm-icon-albicocca-rossa_tree.png",
    "img_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1562324133/fruits/albicocca_rossa_biorfarm_frutta.jpg",
    "number": 37,
    "co2_unit": 0.07,
    "farmer_name": "Famiglia Dell'Orto",
    "location_country": "Piana del Sele, Campania",
    "location_province": "Piana del Sele, Salerno (SA)",
    "selected": true,
    "co2": "2,59"
  },
  {
    "fruit_id": 102377,
    "name": "arancia tarocco gallo",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1608220860/fruits/biorfarm-icon-arancia-tarocco-gallo_tree.png",
    "number": 36,
    "co2_unit": 0.07,
    "farmer_name": "Emanuele Calvo",
    "location_country": "Mineo, Sicilia",
    "location_province": "Mineo, Catania (CT)",
    "selected": true,
    "co2": "2,52"
  },
  {
    "fruit_id": 17811,
    "name": "susina nera",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1561477693/fruits/biorfarm-icon-susina_nera_tree.png",
    "number": 37,
    "co2_unit": 0.024,
    "farmer_name": "Daniele Bucci",
    "location_country": "Faenza, Emilia Romagna",
    "location_province": "Faenza, Ravenna (RA)",
    "selected": true,
    "co2": "0,89"
  }
]