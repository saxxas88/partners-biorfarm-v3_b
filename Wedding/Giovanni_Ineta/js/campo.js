/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees=[
  {
    "fruit_id": 89691,
    "name": "alveare acacia",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1606987616/fruits/biorfarm-icon-alveare-acacia_tree.png",
    "number": 1,
    "co2_unit": 0,
    "farmer_name": "Famiglia Nucci",
    "location_country": "Santarcangelo di Romagna, Emilia-Romagna",
    "location_province": "Santarcangelo di Romagna, Rimini (RN)",
    "selected": true,
    "co2": "0,00"
  },
  {
    "fruit_id": 122329,
    "name": "alveare cardo di montagna",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1616490154/fruits/biorfarm-icon-alveare-cardo-di-montagna_tree.png",
    "number": 1,
    "co2_unit": 0,
    "farmer_name": "Nicolò Lo Piccolo",
    "location_country": "Caltagirone, Sicilia",
    "location_province": "Caltagirone, Catania (CT)",
    "selected": true,
    "co2": "0,00"
  }
]