/****** CO2******/
//  newCO2 = (newNumber * co2) / number

/*************/
const partner_trees = [
  {
    "fruit_id": 74882,
    "name": "mandarino tardivo di ciaculli",
    "icon_tree": "https://res.cloudinary.com/biorfarm/image/upload/v1595409710/fruits/biorfarm-icon-mandarino-tardivo-di-ciaculli_tree.png",
    "number": 20,
    "co2_unit": 0.06077,
    "farmer_name": "Fratelli Marceno",
    "location_country": " Palermo, Sicilia",
    "location_province": "Palermo (PA)",
    "selected": true,
    "co2": "1,22"
  }
]
